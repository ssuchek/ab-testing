from optparse import OptionParser
import sys, os, re, string
sys.path.append('src/')

import PlotStats as ps
import CalcStats as cs
import seaborn as sb
import os

import numpy as np
import scipy.stats as sts
import matplotlib.pyplot as plt

def main():

	exec_path = os.path.abspath(os.path.dirname(sys.argv[0]))

	# Configure parser
	parser = OptionParser(usage="usage: %prog --bcon=462 --bnum=2208 --acon=463 --anum=2199 [--isPooled --oneTail]")
	parser.add_option("--bcon", dest="bcon", type=float, help="baseline number of conversions", default=None)
	parser.add_option("--bnum", dest="bnum", type=int, help="size of baseline sample", default=None)
	parser.add_option("--acon", dest="acon", type=float, help="alternative number of conversions", default=None)
	parser.add_option("--anum", dest="anum", type=int, help="size of alternative sample", default=None)
	parser.add_option("--isPooled", dest="isPooled", action="store_true", help="pooled A/B testing", default=None)
	parser.add_option("--oneTail", dest="oneTail", action="store_true", help="one or two tailed A/B testing", default=None)

	# Parse input arguments
	options, args = parser.parse_args()

	# Args consistency checks
	if not options.bcon:
		print ("ERROR: Missing required argument '--bcon'")
		parser.print_help()
		sys.exit(1)

	if not options.bnum:
		print ("ERROR: Missing required argument '--bnum'")
		parser.print_help()
		sys.exit(1)

	if not options.acon:
		print ("ERROR: Missing required argument '--acon'")
		parser.print_help()
		sys.exit(1)

	if not options.anum:
		print ("ERROR: Missing required argument '--anum'")
		parser.print_help()
		sys.exit(1)

	n0, k0 = options.bnum, options.bcon
	n1, k1 = options.anum, options.acon

	# n0, k0 = 2200, 429
	# n1, k1 = 2228, 426

	# n0, k0 = 2208, 462
	# n1, k1 = 2199, 463

	# n0, k0 = 8500, 1510
	# n1, k1 = 8500, 1410

	# n0, k0 = 2174, 411
	# n1, k1 = 2252, 434

	p0, p1 = k0/n0, k1/n1

	plt.style.use('ggplot')
	plt.rcParams['figure.figsize'] = 12,6

	pic_dir_name = 'pics/bnum{0:.0f}_bcon{1:.0f}_anum{2:.0f}_acon{3:.0f}/'.format(n0,k0,n1,k1)
	if not os.path.exists(pic_dir_name):
		os.makedirs(pic_dir_name)
		print (os.path.abspath(pic_dir_name))
	plt.rcParams["savefig.directory"] = os.chdir(os.path.dirname(pic_dir_name))

	plt.rcParams["savefig.format"] = 'pdf'

	params = params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize': 'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}

	plt.rcParams.update(params)

	IsPooled = options.isPooled
	one_tail = options.oneTail
	show_stat_type = True
	stat_type = 'significance_power'

	ps.plot_control_binom_distr(n0, n1, p0, p1, one_tail = one_tail)
	ps.plot_control_norm_distr(n0, n1, p0, p1)

	ps.plot_ABtest(n0, n1, p0, p1, alpha = 0.05, show_stat_type = show_stat_type, stat_type = stat_type, 
		label = 'ABtest_crd', isSampleSizeTest = False, IsPooled = IsPooled, one_tail = one_tail)

	min_sample_size = ps.plot_min_sample_size(p0, beta = 0.2, alpha = 0.05)

	for dp, size in min_sample_size.items():

		label = 'ABtest_crd_dp{0:.0f}'.format(100*dp)

		ps.plot_ABtest(size, size, p0, (1+dp)*p0, alpha = 0.05, show_stat_type = show_stat_type, stat_type = stat_type, 
			label = label, isSampleSizeTest = True, IsPooled = IsPooled, one_tail = one_tail)

	trans_list0 = [195 for i in range(18)] + [146 for i in range(4)] + [144 for i in range(2)]  + [147 for i in range(3)] + [69] + [0 for i in range(1338)]
	#trans_list1 = [195 for i in range(17)] + [146 for i in range(9)] + [144 for i in range(3)]  + [147 for i in range(2)] + [69] + [0 for i in range(1316)]
	trans_list1 = [195 for i in range(87)] + [146 for i in range(29)] + [144 for i in range(10)]  + [147 for i in range(9)] + [69 for i in range(7)] + [0 for i in range(1200)]

	ps.plot_Mann_Whitney_ranks(trans_list0, trans_list1)

	a0 = [4,2,3,18,1]
	a1 = [9,3,2,17,1]

	atot = np.array([a0, a1])

	chi2_total, chi2_pval, chi2_ndof = ps.plot_multinomial_chi2(atot)

	print (chi2_total, chi2_pval, chi2_ndof)

if __name__ =='__main__':
	main()