#!/usr/bin/env python
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE
from optparse import OptionParser
from glob import glob
import os, sys, re, shlex, operator
from stat import *

def groupFilesBySize(files, maxSize):
    # returns a list [[inputfiles, size]]
   
    # fill files in dictionary
    file_dict = {}
    input_size = 0
    for filename in files:
         #print filename
         if os.path.isfile(filename):
             fn = filename
             while os.path.islink(fn):
                 fn = os.readlink(fn)
             size = os.path.getsize(fn)
             file_dict[filename] = size
             input_size += size
                
    #print "Input contains %d files (%i byte)" % (len(file_dict), input_size)
    #for filename in reversed(sorted(file_dict, key = lambda x: file_dict[x])):
        #print "%12i %s" % (file_dict[filename], filename)

    # sort files into subjobs
    jobs = []
    for filename in reversed(sorted(file_dict, key = lambda x: file_dict[x])):
        filesize = file_dict[filename]
        #print "Adding file %s (%i)" % (filename, filesize)
        if filesize > maxSize * 1073741824:
            raise RuntimeError("ERROR: File size from %s (%i) exceeds maxSize (%i GB)." % (filename, filesize, maxSize))
        
        # adding first element
        if len(jobs) == 0:
            #print "Appending", filename, "(first element)"
            jobs.append((filename, filesize))
        else:
            # check if we can add current file to existing subjob
            # starting with the largest subjob
            added = False
            for subjob in reversed(sorted(jobs, key = lambda x: x[1])):
                if (subjob[1] + filesize) < maxSize:
                    #print "Adding", filename
                    i = jobs.index(subjob)
                    jobs[i] = (subjob[0]+","+filename, subjob[1]+filesize)
                    added = True
                    break
            # check if successfully added
            if not added:
                #print "Appending", filename, "(was not added)"
                jobs.append((filename, filesize))
    return jobs
    
def split_seq_size(files, maxSize):
    # wrapper for direct usage in submit.py
    jobs = groupFilesBySize(files, maxSize)
    s = []
    for i in jobs:
       s.append(i[0].split(","))
    return s

def main():
    """ entry point """

    # configure optparse
    parser = OptionParser(usage="usage: %prog [options] <files.txt>")
    parser.add_option("--maxSize", dest="maxSize", help="Maximal size of input files per subjob.", type=long, default=15000000000)

    options, filename = parser.parse_args()
    
    if not filename:
        parser.print_help()
        raise RuntimeError("ERROR: No filename given, nothing to do.")
    
    # read file list from file
    f = open(filename[0])
    file_list = []
    for l in f:
        file_list.append(l.rstrip("\n"))
        
    # start sorting    
    jobs = groupFilesBySize(file_list, options.maxSize)
    size = 0
    nFiles = 0
    for j, i in enumerate(jobs):
       entries = len(i[0].split(","))
       nFiles += entries
       size += i[1]
       print "%3i: %i files (%i bytes )" % (j, entries, i[1])

    print "SUMMARY: %i files (%i bytes) in %i subjobs." % (nFiles, size, j)


if __name__ == "__main__":
    main()
