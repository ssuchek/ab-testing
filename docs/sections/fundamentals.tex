\chapter{Fundamental concepts}
\label{sec:fund}

\graphicspath{{figs/fundams/}}

\section{Probability}
\label{sec:prob}


Often one has a situation where the outcome of some experiment is unpredictable and changes as one repeats the same experiment multiple times, It means that the results cannot be predicted with 100\% certainty. Such system is then called \textit{random}. The measure of the randomness of the system is called \textit{probability}.

Consider a \textit{sample space} $S$ that consists of some elements. Divide the sample space in subsets $A$, $B$, $C$,..., and assign some real numbers $p(A)$, $p(B)$,... to each subset with the following properties:

\begin{itemize}
	\item For every subset $A \in S$, $p(A) \geq 0$.
	\item For any disjoint ($A \cap B \neq 0$) subsets A and B the probability assigned to the union of $A$ and $B$, $A \cup B$, is the sum of two corresponding probabilities, $p(A \cup B) = p(A) + p(B)$.
	\item Probability assigned to the whole sample space is equal to 1, $p(S) = 1$.
	\item Probability of the complemet of A, $\overline{A}$, $p(\overline{A}) = 1 - p(A)$.
	\item If $A$ is itself a subset of the subset $B$, then $p(A) \leq p(B)$.
	\item $p(A \cup B) = p(A) + p(B) - p(A \cap B)$.
\end{itemize} 

A variable or any measurable function that takes on a certain value for each element of $S$ is referred to as \textit{random variable}. 

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.45\textwidth]{joint_prob}
  \caption[Illustration of the joint probability $p(A \cap B)$.]{Illustration of the joint probability $p(A \cap B)$~\cite{funds:joint_prob}.} 
  \label{fig:joint_prob}
\end{figure}

Consider two subsets $A$ and $B$ of space $S$. Given $p(B) \neq 0$, one can introduce so called \textit{conditional probability of $A$ given $B$}, $p(A | B)$ as

\begin{eqnarray}
	p(A | B) = \frac{p(A \cap B)}{p(B)}\,,
\label{eq:cond_prob_ab}
\end{eqnarray}  

and conditional probability of $B$ given $A$

\begin{eqnarray}
	p(B | A) = \frac{p(B \cap A)}{p(A)}\,,
\label{eq:cond_prob_ba}
\end{eqnarray}  

The subset $A$ is called \textit{independent} of $B$ if $p(A | B) = p(A)$. Using Eq.~\ref{eq:cond_prob} one obtains that 

\begin{eqnarray}
	p(A \cap B) = p(A)p(B)\,.
\label{eq:cond_indep}
\end{eqnarray}  

Return to Eq.~\ref{eq:cond_prob_ab} and \ref{eq:cond_prob_ba}. Given that $p(A \cap B) \equiv p(B \cap A)$

\begin{eqnarray}
	p(A | B) = \frac{p(B | A)p(A)}{p(B)}\,.
\label{eq:cond_prob_bayes}
\end{eqnarray}  

Eq.~\ref{eq:cond_prob_bayes} is referred to as \textit{Bayes' theorem}~\cite{Bayes:1763}. Suppose the space $S$ can be divided into disjoint subsets $A_{i}$. Then the probability assigned to any arbitrary subset $B$ of space $S$ can expressed as

\begin{eqnarray}
	p(B) = \sum\limits_{i}p(B | A_{i})p(A_{i})\,.
\label{eq:total_prob_law}
\end{eqnarray}  

Eq.~\ref{eq:total_prob_law} is referred to as the \textit{law of total probability}. Given the Bayes' theorem one can write

\begin{eqnarray}
	p(A | B) = \frac{p(B | A)p(A)}{\sum\limits_{i}p(B | A_{i})p(A_{i})}\,.
\label{eq:cond_prob_bayes_total}
\end{eqnarray}  




\subsection{Probability as a relative frequency}
\label{subsec:rel_freq}

A \textit{relative frequency} interpretation is used when the possible outcomes in the experimental sample set $S$ are assumed to be \textit{repeatable}. Assign subset $A$ to the occurences of one of the possible outcomes of the measurement. Then generally the probability of the outcome $A$ is defined as a fraction of times that the outcome $A$ occurs in the limit of infinitely many measurements:

\begin{eqnarray}
	P(A) = \lim\limits_{n \rightarrow \infty}\frac{\text{$N_{A}$ of the outcome $A$ in $N$ measurements}}{N}\,.
\label{eq:rel_freq_prob}
\end{eqnarray}  

The total probability of different outcomes then is equal to 1, i.e. $P(S) = \sum\limits_{i = 1,...,N} P(A_{i}) = 1$. It is obvious that one cannot perform infinitely many measurements and therefore determine the probability with 100\% precision. It means that the probability is estimated with some uncertainty given a size of the experimental data and particular model. 

\FloatBarrier

\subsection{Subjective probability}
\label{subsec:subj_prob}

The \textit{subjective} or \textit{Bayesian} interpretation of the probability corresponds to the \textit{hypothesis testing} which is discussed in Chapter~\ref{sec:fund}. The hypothesis then can be associated with a given experimental outcome occuring a certain fraction of time, so that the subjective probability also includes a relative frequency interpretation. This probability is interpreted then as a measure of degree of belief in a certain hypothesis $H$:

\begin{eqnarray}
	P(H) = \text{degree of belief in a hypothesis $H$}\,.
\label{eq:subj_prob}
\end{eqnarray}  

The hypothesis space then is constructed such that all elementary hypotheses $H_{i}$ are mutually exclusive meaning that only one of them is true. This requirement ensures that the total probability is equal to 1, i.e. $P(S) = \sum\limits_{i = 1,...,N} P(H_{i}) = 1$.   

\FloatBarrier

\section{Probability density function}
\label{sec:pdf}

Consider an experiment with an outcome that is characterized by a continuous random variable $x$. Then the sample space corresponds to the certain range of the possible values of $x$. The probability of observing a value of $x$ within an infinitesimal interval $[x, x+dx]$ is given by the \textit{probability density function} or \textit{p.d.f.} $f(x)$:

\begin{eqnarray}
	P(x \in [x, x+dx]) = f(x)dx\,.
	\label{eq:pdf}
\end{eqnarray}

Since the total probability of observing $x$ in its entire range is equal to 1, the p.d.f. is also normalized to 1:

\begin{eqnarray}
	P(x) = \int\limits_{x \in S} P(x \in [x, x+dx]) = \int\limits_{x \in S} f(x)dx = 1\,.
	\label{eq:pdf_norm}
\end{eqnarray}

In case when $x$ takes only discrete values $x_{i},\,i = 1,...,N$ the probability of observing value $x_{i}$ is defined as

\begin{eqnarray}
	P(x_{i}) = f_{i}\,,
	\label{eq:pdf_discr}
\end{eqnarray}

where the normalization of $f_{i}$ is

\begin{eqnarray}
	\sum\limits_{i = 1}^{N} f_{i} = 1\,.
	\label{eq:pdf_discr_norm}
\end{eqnarray}

The probability to observe variable $x$ that is less than or equal $X$ is described by the \textit{cumulative distribution function} or c.d.f.

\begin{eqnarray}
	P(x \leq X) = F(X) = \int\limits_{-\infty}^{X} f(x)dx\,.
	\label{eq:cdf}
\end{eqnarray}

In case of a discrete variable $x_{i}$, the c.d.f. is defined then as

\begin{eqnarray}
	F(X) = \sum\limits_{x_{i} \leq X} f_{i}\,.
	\label{eq:cdf_discr}
\end{eqnarray}

The c.d.f. is tightly related to the \textit{$\alpha-$point} or what is considered later as \textit{significance level $\alpha$}. The $\alpha-$point is defined such that $F(x_{\alpha}) = \alpha$, where $\alpha \in [0, 1]$

\begin{eqnarray}
	x_{\alpha} = F^{-1}(\alpha)\,.
	\label{eq:apoint}
\end{eqnarray}

The point $x_{1/2}$ where the possibilities of observing $x$ less or greater than $x_{1/2}$ are equal is called the \textit{median} of $x$. Another important point is the \textit{mode} of $x$ that denotes the point where the p.d.f. reaches the maximum value.

Consider a case where the result of the measurements is characterized by two continuous quantities, $x$ and $y$. Assign event $X$ for $x$ to be observed in $[x, x+dx]$ and event $Y$ for $y$ to be observed in $[y, y+dy]$. Then the joint p.d.f. $f(x,y)$ or the probability to observe $x$ in $[x, x+dx]$ and $y$ in $[y, y+dy]$ is defined as

\begin{eqnarray}
P(X \cap Y) = P(x \in [x, x+dx],x \in [y, y+dy]) = f(x,y)dxdy\,,
\label{eq:pdf_joint}	
\end{eqnarray} 

where

\begin{eqnarray}
\int\limits_{x \in S}\int\limits_{y \in S}f(x,y)dxdy = 1\
\label{eq:pdf_joint}	
\end{eqnarray} 

\FloatBarrier

\subsection{Expectation value and standard deviation}
\label{subsec:mean_std}

One of the key aspects of the p.d.f. is so-called \textit{expectation value} $E[x]$ of the continuous random variable $x$ that describes the average of the variable given the p.d.f. or more strictly the probability-weighted average of all possible values of the variable $x$

\begin{eqnarray}
	E[x] = \int\limits_{-\infty}^{+\infty} xf(x)dx = \mu\,.
	\label{eq:mu}
\end{eqnarray}

The expectation value can be also denotes as $\mu$ or the \textit{population mean} of the variable $x$. In other words $E(x)$ is a measure of where values of $x$ are most likely to be observed.

More general case of the expectation value is the expectation value of $x^{n}$ or the \textit{$n$th moments} of $x$

\begin{eqnarray}
	E[x^{n}] = \int\limits_{-\infty}^{+\infty} x^{n}f(x)dx = \mu'_{n}\,.
	\label{eq:nmom}
\end{eqnarray}

Another important measure is so-called \textit{$n$th central moment} of $x$ that describes how widely $x^{n}$ is spread about its mean value

\begin{eqnarray}
	E[(x - E(x))^{n}] = \int\limits_{-\infty}^{+\infty} (x - E[x])^{n}f(x)dx = \mu_{n}\,.
	\label{eq:ncentmom}
\end{eqnarray}

The first central moments have the following interpretations:
\begin{itemize}
	\item The 0th central moment: $\mu_{0} = 1$.
	\item The 1th central moment: $\mu_{1} = E[(x - E[x])] = \int\limits_{-\infty}^{+\infty} (x - \mu)f(x)dx = 0$.
	\item The 2nd central moment: $V[x] = E[(x - E[x])^{2}] = \int\limits_{-\infty}^{+\infty} (x - \mu)^{2}f(x)dx = E[x^{2}] - \mu^{2}$.
\end{itemize}

The 2nd moment or \textit{population variance} is a particularly important quantity. The square root of the variance or the \textit{standard deviation}, $\sigma$, of $x$ describes how widely the random variable $x$ is spread about its mean value $\mu$. The 3rd and 4th moments are related to the \textit{skewness} and \textit{kurtosis} which are the different measures of the asymmetry of the p.d.f. of the random variable $x$ about its mean.

In case of a discrete random variable $x$ and finite number $n$ of the observations, the above relations can be rewritten as follows

\begin{eqnarray}
\begin{aligned}
	E[x] &= \sum\limits_{i = 1}^{n} p_{i}x_{i} = \mu \\
	V[x] &= E[x^{2}] - \mu^{2} = \sum\limits_{i = 1}^{n} p_{i}x^2_{i} - 2 \mu \sum\limits_{i = 1}^{n} p_{i}x_{i} + \sum\limits_{i = 1}^{n} p_{i} \mu^{2} = \sum\limits_{i = 1}^{n} p_{i}(x_{i} - \mu)^2 \,,
	\label{eq:muvar_discr}
\end{aligned}
\end{eqnarray}

given that $\sum\limits_{i = 1}^{n} p_{i} = 1$. In case when the probabilities $p_{i}$ to observe the value $x_{i}$ are equal, $p_{i} = 1/n$, then the above equations become

\begin{eqnarray}
\begin{aligned}
	E[x] &= \frac{1}{n}\sum\limits_{i = 1}^{n} x_{i} = \mu \\
	V[x] &= \frac{1}{n}\sum\limits_{i = 1}^{n} (x_{i} - \mu)^2 \,.
	\label{eq:muvar_discr_eqprob}
\end{aligned}
\end{eqnarray}

The variance $V[x]$ can be also expressed in terms of not the population mean but the distance between the values $x_{i}$

\begin{eqnarray}
\begin{aligned}
	V[x] &= \frac{1}{n^2}\sum\limits_{i = 1}^{n}\sum\limits_{i = j}^{n} \frac{(x_{i} - x_{j})^2}{2} \,.
	\label{eq:muvar_discr_eqprob_dsit}
\end{aligned}
\end{eqnarray}

One important property of the variance is that in case of the uncorrelated random variables $y_{i}$ the variance of the sum of the variables is equal to the sum of the variances (so-called Bienaymé formula)

\begin{eqnarray}
\begin{aligned}
	V\left[\sum\limits_{i = 1}^{n} y_{i}\right] &= \sum\limits_{i = 1}^{n}V\left[y_{i}\right] \,.
	\label{eq:muvar_discr_eqprob_sum}
\end{aligned}
\end{eqnarray}

It actually means that the variance of the average of all variables in case of the equal variances $V\left[y_{i}\right] = \sigma^2$

\begin{eqnarray}
\begin{aligned}
	V\left[\frac{1}{n}\sum\limits_{i = 1}^{n} y_{i}\right] &= \frac{1}{n^2}\sum\limits_{i = 1}^{n}V\left[y_{i}\right] = \frac{\sigma^2}{n} \,.
	\label{eq:muvar_discr_eqprob_av}
\end{aligned}
\end{eqnarray}

Consider now a case when the result of the measurements is characterized by two independent random variables, $x$ and $y$, the expectation value takes form

\begin{eqnarray}
	E[xy] = E[xy] = \mu_{x}\mu_{y}\,.
	\label{eq:mu_xy}
\end{eqnarray}

Another important case is when the random variables are not independent but somehow correlated. Then a measure of the joint variability of two random variables, $x$ and $y$, is the \textit{covariance}

\begin{eqnarray}
	\cov(x,y) = E[(x - E[x])(y - E[y])] = E[(x - \mu_{x})(y - \mu_{y})] = E[xy] - \mu_{x}\mu_{y}\,.
	\label{eq:cov}
\end{eqnarray}

One can see that if the variables are independent then $E(xy) = \mu_{x}\mu_{y}$ and thus $\cov(x,y) = 0$. A measure of the level correlation between random variables is called the \textit{correlation coefficient}

\begin{eqnarray}
	\rho_{xy} = \frac{\cov(x,y)}{\sigma_{x}\sigma_{y}}\,,
	\label{eq:corr_coef}
\end{eqnarray}

where $\sigma_{x}$ and $\sigma_{y}$ are the standard deviations of random variables $x$ and $y$ respectively. The correlation coefficient takes value between -1 and 1 with -1 meaning full anticorrelation, 0 meaning no correlation and 1 meaning full corellation.

Consider that instead of random variables $x$ and $y$ some function of them, $g(x,y)$, is measured in the experiment. Then the variance of the following function takes a form

\begin{eqnarray}
	\sigma^{2}_{g} = E[(g - E[g])^{2}] = \sum\limits_{a,b = x,y} \left[ \frac{\partial g}{\partial a} \frac{\partial g}{\partial b} \right]_{\substack{x = \mu_{x} \\ y = \mu_{y}}} \cov(a,b)\,.
	\label{eq:var_func_cor}
\end{eqnarray}

In case of no correlation between the random variables $x$ and $y$, the joint standard deviation becomes

\begin{eqnarray}
	\sigma^{2}_{g} = E[(g - E[g])^{2}] = \sum\limits_{a = x,y} \left[ \frac{\partial g}{\partial a} \right]^{2}_{\substack{x = \mu_{x} \\ y = \mu_{y}}} \sigma^{2}_{a}\,.
	\label{eq:var_func}
\end{eqnarray}

In case of the simplest functions $g = x \pm y$ and $g = xy$ the uncorrelated joint standard deviation takes the form

\begin{eqnarray}
	\begin{aligned}
		\sigma^{2}_{x \pm y} &\approx \sigma^{2}_{x} + \sigma^{2}_{y} \\
		\frac{\sigma^{2}_{xy}}{(xy)^2} &= \frac{\sigma^{2}_{x}}{x^2} + \frac{\sigma^{2}_{y}}{y^2} \,.
	\end{aligned}
	\label{eq:var_func_sum_cor}
\end{eqnarray}

\FloatBarrier

\subsection{Binomial and multinomial distributions}
\label{subsec:binom}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.48\textwidth]{binom_100}
  \includegraphics[width=0.48\textwidth]{binom_250} \\
  \includegraphics[width=0.48\textwidth]{binom_500}
  \includegraphics[width=0.48\textwidth]{binom_1000}
  \caption[Binomial distribution of the number of "successes" $k$ for different numbers $n = 100, 250, 500, 1000$ of trials.]{Binomial distribution of the number of "successes" $k$ given the probability of the "success" $p = 0.5$ for different numbers $n = 100, 250, 500, 1000$ of trials.} 
  \label{fig:binom}
\end{figure}

Consider a set of $n$ independent trials (so called \textit{Bernoulli trials}), each having two possible outcomes, "success" and "failure". Denote $p$ as a probability for a "success" type of outcome. Such set of trials can be considered as a single experiment which is characterized by a discrete random variable $k$, defined as a total number of "success" outcomes. If this experiment is repeated many times, than the relative frequency of having $k$ successful outcomes is described by so called \textit{binomial} distribution

\begin{eqnarray}
	f(k ; n, p) = \frac{n!}{k!(n-k)!} p^{k} (1-p)^{n-k}, \qquad k = 0,...,n\,.
	\label{eq:binom}
\end{eqnarray}

The choice of a binomial distribution can be described as follows. Since the trials are independent then the probability of having $l$ successes and $m$ failures is a product of independent probabilities, $p^{l}*(1-p)^{m}$. Since the total number of successes is $k$ the total numbers of failures is $n-k$ which gives the total probability of $p^{k}*(1-p)^{n-k}$. One cannot distinguish one successful outcome from another and same applies to failures. It means that the one particular order of successes and failures does not matter but only the total numbers. The total number of possible sequences with $k$ successes in $n$ events is described by the binomial coefficient

\begin{eqnarray}
	\binom{n}{k} = \frac{n!}{k!(n-k)!}\,,
	\label{eq:binom_coef}
\end{eqnarray}

which together with the total probability gives the binomial distribution in Eq.~\ref{eq:binom}.

The expectation value and the variance of the binomial distribution according to Eq.~\ref{eq:mu} and \ref{eq:ncentmom} are

\begin{eqnarray}
\begin{aligned}
	E[k] &= \sum\limits_{k = 0}^{+\infty}k\frac{n!}{k!(n-k)!}p^{k} (1-p)^{n-k} = np \\
	V[k] &= E[k^{2}] - (E[k])^{2} = np(1-p)
	\label{eq:muvar_binom}
\end{aligned}
\end{eqnarray}

The generalized case of more than two outcomes in the trial is described by the \textit{multinomial} distribution. Consider again a set of $n$ independent trials, but now each having $m$ possible outcomes with probability $p_{i}$ assigned to the particular type of outcome $i$. Then following the same line of thoughts as in case of two outcomes, one can find that the joint outcome of $k_{i}$ outcomes of type $i$ is given by the following distibution: 

\begin{eqnarray}
	f(k_{1},...,k_{m} ; n, p_{1},...,p_{m}) = \frac{n!}{k_{1}!k_{2}!...k_{m}!} p_{1}^{k_{1}} p_{2}^{k_{2}}...p_{m}^{k_{m}}\,.
	\label{eq:multinom}
\end{eqnarray}
 
The expectation values and variances of individual outcomes are the same as in case of two-outcome experiment

\begin{eqnarray}
\begin{aligned}
	E[k_{i}] &= np_{i} \\
	V[k_{i}] &= np_{i}(1-p_{i})
	\label{eq:muvar_multinom}
\end{aligned}
\end{eqnarray}

\FloatBarrier

\subsection{Poisson distribution}
\label{subsec:poisson}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.48\textwidth]{poisson_10}
  \includegraphics[width=0.48\textwidth]{poisson_50} \\
  \includegraphics[width=0.48\textwidth]{poisson_100}
  \includegraphics[width=0.48\textwidth]{poisson_1000}
  \caption[Poisson distribution of the random integer variable $n$ for different values of parameter $\nu = 10, 50, 100, 1000$.]{Poisson distribution of the random integer variable $n$ for different values of parameter $\nu = 10, 50, 100, 1000$.} 
  \label{fig:poisson}
\end{figure}

Consider the binomial distribution described in Section~\ref{subsec:binom} in the limit $n \gg 1$ and $p \rightarrow 0$ but with the finite value of $np = \nu$. Then the binomial distribution given by Eq.~\ref{eq:binom} converge in this limit to the \textit{Poisson dsitribution} of the random integer variable $n = 0,1,...,\infty$ with the parameter $\nu$                                      

\begin{eqnarray}
	f(n ; \mu) = \frac{\nu^n}{n!} e^{-\nu}\,.
	\label{eq:poisson}
\end{eqnarray}

One can calculate its expectation value and variance according to Eq.~\ref{eq:muvar_discr} as

\begin{eqnarray}
\begin{aligned}
	E[x] &= \sum\limits_{n = 0}^{\infty} n \frac{\nu^n}{n!} e^{-\nu} = \nu \\
	V[x] &= E[x^{2}] - \nu^{2} = \sum\limits_{n = 0}^{\infty} (n - \nu)^2 \frac{\nu^n}{n!} e^{-\nu} \ \nu\,,
	\label{eq:muvar_pois}
\end{aligned}
\end{eqnarray}

meaning that its standard deviation $\sigma$ is equal to $\sqrt{\nu}$.

\FloatBarrier

\subsection{Gaussian distribution}
\label{sec:norm}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.9\textwidth]{gaussian}
  \caption[Gaussian distribution of the variable $x$.]{Gaussian distribution of the variable $x$. Coloured lines represent the mean $\mu$ of the distribution, one and two standard deviations from the mean, $\mu \pm \sigma$ and $\mu \pm 2\sigma$, respectively.} 
  \label{fig:norm}
\end{figure}

The \textit{Gaussian} or \textit{normal} distribution is defined for the continuous random variable $x$ as

\begin{eqnarray}
	f(x ; \mu, \sigma) = \frac{1}{\sqrt{2\pi\sigma}}\,e^{-\frac{(x-\mu)^{2}}{2\sigma^{2}}}\,,
	\label{eq:norm}
\end{eqnarray}

where $\mu$ and $\sigma$ are the mean and standard deviation respectively. The names of the parameters are clearly motivated by their relations with the expectation value and variance:

\begin{eqnarray}
\begin{aligned}
	E[x] &= \int\limits_{-\infty}^{+\infty} x \frac{1}{\sqrt{2\pi\sigma}}\,e^{-\frac{(x-\mu)^{2}}{2\sigma^{2}}} = \mu \\
	V[x] &= \int\limits_{-\infty}^{+\infty} (x-\mu)^{2} \frac{1}{\sqrt{2\pi\sigma}}\,e^{-\frac{(x-\mu)^{2}}{2\sigma^{2}}} = \sigma^{2}\,.
	\label{eq:muvar_norm}
\end{aligned}
\end{eqnarray}

A very special case of the normal distribution which is worth mentioning is the \text{standard} normal distribution with $\mu = 0$ and $\sigma = 1$

\begin{eqnarray}
	\phi(x) = \frac{1}{\sqrt{2\pi}}\,e^{-\frac{x^{2}}{2}}\,,
	\label{eq:stdnorm}
\end{eqnarray}

with the corresponding c.d.f. 

\begin{eqnarray}
	\Phi(x) = \int\limits_{-\infty}^{x'}\frac{1}{\sqrt{2\pi}}\,e^{-\frac{x'^{2}}{2}}dx\,.
	\label{eq:stdnormcdf}
\end{eqnarray}

It can be show that if random variable $x$ is normally distributed then the variable $z$

\begin{eqnarray}
	z = \frac{x - \mu}{\sigma}\,,
	\label{eq:zscore}
\end{eqnarray}

is distributed according to the standard normal distribution with c.d.f. related by $F(x) = \Phi(z)$. Since there is no analytical expression for the standard cumulative distribution it has to be computed numerically. The corresponding $\alpha-$points (see Eq.~\ref{eq:apoint}), $z_{\alpha} = \Phi^{-1}(\alpha)$, are directly related to the significance level discussed in the next chapter.

The normal distribution is particularly useful to describe real-valued random variables whose distributions are unknown because of the \textit{central limit theorem}. It states that under certain conditions the distribution of the average of samples of observations of random variables converge to the normal distribution in the limit of large number of observations even if variables themselves are not normally distributed.

\FloatBarrier

\subsection{Normal approximation to binomial distribution}
\label{subsec:norm_approx_binom}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.9\textwidth]{binom_approx_200}
  \caption[Normal approximation to the binomial distribution.]{Normal approximation to the binomial distribution with the mean $\mu$ and standard deviation $\sigma$ given by Eq.~\ref{eq:muvar_binom} for $n = 200$ of trials.} 
  \label{fig:norm_approx_binom}
\end{figure}

The binomial distribution can be approximated by the normal distribution with the mean and standard deviation given by Eq.~\ref{eq:muvar_binom} in the limit of the large number $n$ of trials. One can also approximate the binomial distribution in terms of the probability $p$ of the "success" outcome with the following mean and standard deviation 

\begin{eqnarray}
\begin{aligned}
	(\mu,\,\sigma) &= \left(p,\,\frac{p(1-p)}{n}\right)\,.
	\label{eq:norm_approx_binom}
\end{aligned}
\end{eqnarray}

\FloatBarrier

\subsection{$\chi^2$ distribution}
\label{sec:chi2}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.75\textwidth]{chi2_distr}
  \caption[$\chi^2$ distribution of the random variable $x$ for different values of parameter $n = 1, 2, 5, 10$.]{$\chi^2$ distribution of the random integer variable $n$ for different values of parameter $n = 1, 2, 5, 10$.} 
  \label{fig:poisson}
\end{figure}

The $\chi^2$ distribution of the continuous random variable $x \geq 0$ given the \textit{number of degrees of freedom $n = 1,2,3,...,$} (ndf) is defined as

\begin{eqnarray}
	f(x ; n) = \frac{1}{2^{n/2}\Gamma(n/2)} x^{n/2 - 1} e^{-x / 2}\,.
	\label{eq:chi2_distr}
\end{eqnarray}

The expectation value and variance of the $\chi^2$ distribution are 

\begin{eqnarray}
\begin{aligned}
	E[x] &= \int\limits_{0}^{\infty}\frac{x}{2^{n/2}\Gamma(n/2)} x^{n/2 - 1} e^{-x / 2} = n \\
	V[x] &= \int\limits_{0}^{\infty}\frac{(x-n)^2}{2^{n/2}\Gamma(n/2)} x^{n/2 - 1} e^{-x / 2} = 2n\,.
\end{aligned}
\label{eq:muvar_chi2}
\end{eqnarray}

The $\chi^2$ distribution function is particularly important in case of the sum of squares of independent variables described by the Gaussian distribution. Consider $n$ of such variables each described by the Gaussian distribution with mean $\mu_i$ and variance $\sigma_i^2$. Construct the variable $\chi^2$ as

\begin{eqnarray}
	\chi^2 = \sum\limits_{i = 1}^{n} \frac{(x_i - \mu_i)^2}{\sigma_i^2}\,.
	\label{eq:chi2_var}
\end{eqnarray}

The variable $\chi^2$ then follows the $\chi^2$ distribution with $n$ degrees of freedom. Thus the $\chi^2$ distribution is important in so called \textit{goodness-of-fit} tests described in Section~\ref{subsec:gof}.

\FloatBarrier


