\chapter{Statistical tests}
\label{sec:stat_test}

\graphicspath{{figs/stat_test/}}

\section{Hypotheses testing}
\label{sec:hypo_test}

The general goal of the statistical test is to make a statement about how well the observed data agrees with some prediction or how well some alternative observations agrees with the baseline observations. Such statement is made in the form of \textit{hypothesis}. The hypothesis under consideration, i.e. our prediction or baselime measurement, is referred to as the \textit{null hypothesis}, $H_{0}$, which usually specifies the probability density function (p.d.f.), $f(x|H_{0})$, for a measured random variable $x$. 
The validity of the null hypothesis can be established using so-called \textit{alternative hypotheses}, $H_{1}$, $H_{2}$,..., which specify the p.d.f., $f(x|H_{1})$, $f(x|H_{2})$,..., for the observation or some alternative measurement. Besides the probability distributions, one needs to introduce the measure of agreement between the observed data and a given hypothesis which is some function of the measured random variables, referred to as \textit{test statistic} $t(x)$. Since test statistic is a function of the random variables, it is also described by some p.d.f., $g(t|H_{0})$, $g(t|H_{1})$,..., under the assumption of a given hypothesis, $H_{0}$, $H_{1}$,...

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.9\textwidth]{hypothesis_demo}
  \caption[Probability distribution functions for the test statistic under the assumption of different hypotheses.]{Probability distribution functions for the test statistic under the assumption of the null (blue) and alternative (red) hypotheses. The null hypothesis $H_{0}$ is accepted in the region below $t_{\text{cut}}$ and rejected in the region above $t_{\text{cut}}$.} 
  \label{fig:hypo_demo}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.48\textwidth]{alpha}
  \includegraphics[width=0.48\textwidth]{beta}
  \caption[Probability distribution function of the test statistic $t$.]{Probability distribution function of the test statistic $t$ with significance level shown as shaded area on the left plot and statistical power shown as shaded area on the right plot.} 
  \label{fig:alpha_beta}
\end{figure}


Consider some test statistic $t(x)$ under the assumptions of the null and alternative hypothesis, $H_{0}$ and $H_{1}$. The corresponding p.d.f. $g(t|H_{0})$ and $g(t|H_{1})$ are shown on Fig.~\ref{fig:hypo_demo}. Consider two statements in terms of the hypothesis testing, "reject $H_{0}$" and "accept $H_{0}$". The decision on what statement is true is made by defining the critical value of the test statistic, $t_{\text{cut}}$. The regions below and above the critical value, $t < t_{\text{cut}}$ and $t > t_{\text{cut}}$ are called the \textit{acceptance region} and \textit{critical region} respectively. The choice of the critical value is driven by the probability $\alpha$ of observing test statistic at this value, referred to as \textit{significance level}

\begin{eqnarray}
\alpha = \int\limits_{t_{\text{cut}}}^{+\infty} g(t | H_{0}) dt\,.
\label{eq:sig_level}
\end{eqnarray}

Since the hypothesis $H_{0}$ is accepted below the critical value $t_\textit{cut}$, the \textit{probability to reject $H_{0}$ if $H_{0}$ is true} is $\alpha$, $P(reject H_{0} | H_{0} is true) = \alpha$. Such kind of probability to reject the null hypothesis is called the \textit{type I error}. On the other hand, there is some probability, $\beta$, to accept $H_{0}$ if not $H_{0}$ is true but some alternative hypothesis $H_{1}$. This probability then is referred to as \textit{type II error} and is

\begin{eqnarray}
\beta = \int\limits_{-\infty}^{t_{\text{cut}}} g(t | H_{1}) dt\,.
\label{eq:beta}
\end{eqnarray}

The probability $1-\beta$ is called \textit{power} of the statistical test and is used to discriminate against the alternative hypothesis $H_{1}$. 

\FloatBarrier

\section{Parameter estimation and errors}
\label{sec:par_est}

Consider a single observation of the random variable $x$ described by the p.d.f. $f(x)$. A set of $n$ independent observations of the variable $x$ is then called the \textit{sample} of size $n$. Each observation produces a single observed value of $x$, $x_{i}$. Consider now these $n$ independent measurements as a single experiment that is characterized by the vector of single observations, $\mathbf{x} = (x_{1},...,x_{n})$. Since all $n$ measurements are independent and described by the same p.d.f., the joint p.d.f. for the experiment can be expressed using the p.d.f. of each separate measurement

\begin{eqnarray}
f(\mathbf{x}) = f(x_{1})f(x_{2})...f(x_{n})\,.
\label{eq:pdf_joint}
\end{eqnarray}

Sometimes the exact p.d.f. $f(x_{i})$ is unknown and its properties are extracted from the observations of the variable $x$. Quite often the form of the p.d.f. $f(x,\mathbf{\theta})$ is known but the parameters $\mathbf{\theta}$ that enter p.d.f. remain unknown, i.e. mean or variance. Here one needs to construct some function of observations $x_{i}$ to estimate unknown parameters, $\mathbf{\theta} = \mathbf{\theta}(x_1,...,x_n)$. The function $\mathbf{\theta}(x_1,...,x_n)$ is then called a \textit{stastic} if it contains no unknown parameters. If the statistic is used to estimate the property of the p.d.f., it is called an \textit{estimator}, $\hat{\theta}$. The estimator is considered \textit{consistent} if $\hat{\theta}$ converges to $\theta$ in the limit of large $n$:

\begin{eqnarray}
\lim\limits_{n \rightarrow \infty} (P|\hat{\theta} - \theta| > \epsilon) = 0\,,
\label{eq:estimator_consistency}
\end{eqnarray}

where $P|\hat{\theta} - \theta|$ is probability to have $\hat{\theta}$ different from $\theta$ and $\epsilon > 0$ is positive infinitesimal value.

The estimator $\hat{\theta}(x_1,...,x_n)$ is a function of the measured value of a random variable and thus is itself a random variable. Consider repeating the experiment many times. Then the estimator as the random variable is described by the p.d.f. $g(\hat{\theta}; \theta)$ called the \textit{sampling distribution}.

One important measure of the quality of the estimator is the \textit{bias} which is defined as the distance of the expectation value of the estimator of parameter $\theta$ from the $\theta$ itself

\begin{eqnarray}
b = E[\hat{\theta}] - \theta\,.
\label{eq:est_bias}
\end{eqnarray}

The expectation value of the estimator is the function that does not depend on the measured values $x_i$. Thus the bias itself is also independent of $x_i$ but rather dependent on the sample size $n$. The estimator with the bias equal to 0 is called \textit{unbiased}; if the bias is non-zero but converges to 0 for $n \rightarrow \infty$ it is called \textit{asymptotically unbiased}. 

Another important measure of the quality is the \textit{mean squared error} (MSE), defined as

\begin{eqnarray}
\text{MSE} = E[(\hat{\theta}-\theta)^2] = V[\hat{\theta}] + b^2\,,
\label{eq:est_MSE}
\end{eqnarray}

where $V[\hat{\theta}]$ is the variance of the estimator. 

Suppose one did an experiment which resulted in the observed value of estimator of some parameter, $\hat{\theta}_{\text{obs}}$. The observed estimator of the standard deviation of parameter, $\hat{\sigma}_{\hat{\theta}_{\text{obs}}}$, shows then how widely the estimator would be spread if identical experiment were to be repeated many times. Thus, $\hat{\sigma}_{\hat{\theta}_{\text{obs}}}$ is then classified as \textit{statistical uncertainty} of the measurement and referred to as the \textit{standard error}.

Unlike the variance, the bias $b$ is unrelated to the properties of the statistical model and p.d.f. itself but related to our choice of the estimator. Thus the bias is classified as \textit{systematic error}. 

\FloatBarrier

\subsection{Mean and variance estimation}
\label{subsec:mu_std_est}

Suppose that the form of the p.d.f. $f(x)$ is completely unknown. One would like to construct some function of the observed values $x_{i}$ that can be a good estimator for the expectation value of $x$. The simplest possible estimator is the arithmetic mean of the $x_{i}$

\begin{eqnarray}
\xav = \frac{1}{n}\sum\limits_{i = 1}^{n} x_i\,.
\label{eq:mean_est}
\end{eqnarray}

The estimator for the mean in the form of arithmetic mean is called the \textit{sample mean}. One can see that the expectation value of the sample mean is

\begin{eqnarray}
E[\xav] = \frac{1}{n}\sum\limits_{i = 1}^{n} E[x_i] = \mu \,,
\label{eq:mean_est_exp}
\end{eqnarray}

meaning that \xav is an unbiased estimator of the population mean $\mu$ (see Eq.~\ref{eq:muvar_discr_eqprob}).

The corresponding \textit{sample variance} is defined as

\begin{eqnarray}
\begin{aligned}
s^{2} &= \frac{1}{n-1}\sum\limits_{i = 1}^{n} (x_i - \xav)^2 = \frac{1}{n-1}\left[n\left(\frac{1}{n}\sum\limits_{i = 1}^{n} x^2_i\right) - 2n\xav\left(\frac{1}{n}\sum\limits_{i = 1}^{n} x_i \right)  + n(\xav)^2 \right] \\
	 &= \frac{n}{n-1} \left( \xsqav - (\xav)^{2} \right) \,.
\end{aligned}
\label{eq:var_est}
\end{eqnarray}

One can proove that if measurements are independent, then $E[s^2] = \sigma^2$ meaning that $s^{2}$ is an unbiased estimator for the population variance (see Eq.~\ref{eq:muvar_discr_eqprob}).


\section{Confidence belt and interval}
\label{sec:CI_CB}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.48\textwidth]{CI_esimator}
  \includegraphics[width=0.48\textwidth]{CB_esimator}
  \caption[Probability distribution function $g(\hat{\theta}; \theta)$ for a given value of the parameter $\theta$ and corresponding confidence belt of the estimator $\hat{\theta}$]{Probability distribution function $g(\hat{\theta}; \theta)$ for a given value of the parameter $\theta$ (left) and corresponding confidence belt of the estimator $\hat{\theta}$ (right). The left shaded area on the left plot corresponds to the probability $\beta$, while the right shaded area to the significance level $\alpha$. Thick solid black line between the functions $\hat{\theta}_{\alpha}$ and $\hat{\theta}_{\beta}$ represents the confidence interval $[a, b]$ that corresponds to the confidence belt of $\hat{\theta}$.} 
  \label{fig:CI_CB_demo}
\end{figure}


Consider the same experiment discussed in Section~\ref{sec:par_est} and observed some value of the estimator $\hat{\theta}_{\text{obs}}$. Suppose the form of the p.d.f. of the estimator $g(\hat{\theta}; \theta)$ is known meaning that one can calculate the exact p.d.f. for a given value of $\theta$. Consider certain significance level $\alpha$ to observe $\hat{\theta}_{\text{obs}} < \hat{\theta}_{\alpha} (\alpha)$ and probability $\beta$ to observe $\hat{\theta}_{\text{obs}} > \hat{\theta}_{\beta} (\alpha)$. Generally speaking, $\hat{\theta}_{\alpha}$ and $\hat{\theta}_{\beta}$ are the functions of the true value of parameter $\theta$. According to Eq.~\ref{eq:sig_level} and \ref{eq:beta}

\begin{eqnarray}
\begin{aligned}
\alpha &= \int\limits_{\hat{\theta}_{\alpha}(\theta)}^{+\infty} g(\hat{\theta}_{\alpha}(\theta); \theta) d\hat{\theta}  = 1 - G(\hat{\theta}_{\alpha}(\theta); \theta) \\
\beta  &= \int\limits_{-\infty}^{\hat{\theta}_{\beta}(\theta)} g(\hat{\theta}_{\beta}(\theta); \theta) d\hat{\theta}  = G(\hat{\theta}_{\beta}(\theta); \theta)\,,
\end{aligned}
\label{eq:alphabeta_CI}
\end{eqnarray}

where $G$ correponds to the cumulative distribution of the p.d.f. $g$. The region between the functions $\hat{\theta}_{\alpha}$ and $\hat{\theta}_{\beta}$ is called the \textit{confidence belt} which describes the region of the space which contains the estimator regardless of the exact value of $\theta$ with the probability

\begin{eqnarray}
P (\hat{\theta}_{\beta}(\theta) \leq \hat{\theta} \leq \hat{\theta}_{\alpha}(\theta)) = 1 - \alpha - \beta\,.
\label{eq:conf_belt_prob}
\end{eqnarray}

If $\hat{\theta}$ is a good consistent estimator of $\theta$, then in general $\hat{\theta}_{\alpha}$ and $\hat{\theta}_{\beta}$ are monotonically increasing functions of $\theta$. It means that one can define the inverse functions, $a(\theta) \equiv \hat{\theta}_{\alpha}^{-1}$ and $b(\theta) \equiv \hat{\theta}_{\beta}^{-1}$, and rewrite Eq.~\ref{eq:conf_belt_prob} as

\begin{eqnarray}
\begin{aligned}
P (a(\hat{\theta}) \leq \theta \leq b(\hat{\theta})) = 1 - \alpha - \beta\,.
\end{aligned}
\label{eq:conf_belt_prob_1}
\end{eqnarray}

The value of the estimator is determined in the experiment, and thus one can get the actual values $a(\hat{\theta}_{\text{obs}} \equiv a$ and $b(\hat{\theta}_{\text{obs}} \equiv b$. The interval $[a, b]$ is called the \textit{confidence interval} which is defined at a \textit{confidence level} $1 - \alpha - \beta$ regardless of the true value of $\theta$. Note that it does not mean the probability of observing the true value in the confidence interval. It means that if the experiment were to be repeated many times the confidence interval would contain the true value of $\theta$ in a fraction $1 - \alpha - \beta$ of all experiments. The construction of the confidence belt and the corresponding confidence interval of the parameter $\theta$ is shown on Fig.~\ref{fig:CI_CB_demo}.

The regular confidence interval is a \textit{two-sided} interval, meaning that both $a$ and $b$ are specified. One can also specify \textit{one-sided confidence limit} which can be a lower limit $a$ on the parameter $\theta$ such that $\theta \geq a$ with the probability $1 - \alpha$ or the upper limit $b$ such that $\theta \leq b$ with the probability $1 - \beta$.

One particular case of the two-sided intervals is such that $\alpha = \beta \equiv \alpha$. Such interval is called \textit{central confidence interval} with the associated probability $1 - \alpha$.

Given the mentioned above, Eq.~\ref{eq:alphabeta_CI} can be rewritten in terms of $a$ and $b$

\begin{eqnarray}
\begin{aligned}
\alpha & = 1 - G(\hat{\theta}_{\text{obs}}; a) \\
\beta  & = G(\hat{\theta}_{\text{obs}}(\theta); b)\,.
\end{aligned}
\label{eq:alphabeta_CI_1}
\end{eqnarray}

\FloatBarrier

\subsection{Gaussian distributed estimator and confidence interval}
\label{sec:CI_CB_norm}

\begin{table}[!htb]	
		\begin{center}
			\normalsize{
			\begin{tabular}{c|c|c|c}			
				\hline
				\hline
				\multicolumn{2}{c|}{One-sided} & \multicolumn{2}{c}{Two-sided} \\
				\hline
					$\Phi^{-1}(1 - \alpha)$ & $1 - \alpha$ & $\Phi^{-1}(1 - \alpha/2)$ & $1 - \alpha/2$ \\
				\hline
					1 & 0.841 & 1 & 0.683 \\
					2 & 0.977 & 2 & 0.954 \\
					3 & 0.999 & 3 & 0.997 \\
				\hline
				\hline	
		\end{tabular}
		}
		\end{center}
		\caption[Values of the confidence level for different integer values of the $\Phi^{-1}$ of the standard Gaussian distribution]{Values of the confidence level for different integer values of the $\Phi^{-1}$ of the standard Gaussian distribution. Left column shows values for the one-sided right intervals, while right columns shows values for the two-sided central intervals.}
	\label{tab:integer_phi}
\end{table}

\begin{table}[!htb]	
		\begin{center}
			\normalsize{
			\begin{tabular}{c|c|c|c}			
				\hline
				\hline
				\multicolumn{2}{c|}{One-sided} & \multicolumn{2}{c}{Two-sided} \\
				\hline
					$1 - \alpha$ & $\Phi^{-1}(1 - \alpha)$& $1 - \alpha/2$ & $\Phi^{-1}(1 - \alpha/2)$ \\
				\hline
					0.90 & 1.282 & 0.90 & 1.645 \\
					0.95 & 1.645 & 0.95 & 1.960 \\
					0.99 & 2.326 & 0.99 & 2.576 \\
				\hline
				\hline	
		\end{tabular}
		}
		\end{center}
		\caption[Values of the $\Phi^{-1}$ of the standard Gaussian distribution for different values of the confidence level (90\%, 95\% and 99\%).]{Values of the $\Phi^{-1}$ of the standard Gaussian distribution for different values of the confidence level (90\%, 95\% and 99\%). Left column shows values for the one-sided right intervals, while right columns shows values for the two-sided central intervals.}
	\label{tab:cl_phi}
\end{table}

Consider the case when estimator is distributed according to the Gaussian distribution about the value of the parameter $\theta$ with known standard deviation $\sigma_{\hat{\theta}}$

\begin{eqnarray}
	g(\hat{\theta}; \theta, \sigma_{\hat{\theta}}) = \frac{1}{\sqrt{2\pi\sigma_{\hat{\theta}}}}\,e^{-\frac{(\hat{\theta}-\theta)^{2}}{2\sigma_{\hat{\theta}}^{2}}}\,.
	\label{eq:est_norm}
\end{eqnarray}

Let $\hat{\theta}_{\text{obs}}$ be the value of $\hat{\theta}$ observed in the experiment. Given the definition of $\alpha$ and $\beta$ in Eq.~\ref{eq:alphabeta_CI_1} and Eq.~\ref{eq:stdnormcdf}, the confidence interval can be derived from the probabilities $\alpha$ and $\beta$ as follows

\begin{eqnarray}
\begin{aligned}
\alpha & = 1 - G(\hat{\theta}_{\text{obs}}; a, \sigma_{\hat{\theta}}) = 1 - \Phi\left(\frac{\hat{\theta}_{\text{obs}} - a}{\sigma_{\hat{\theta}}}\right)\\
\beta  & = G(\hat{\theta}_{\text{obs}}(\theta); b, \sigma_{\hat{\theta}}) = \Phi\left(\frac{\hat{\theta}_{\text{obs}} - b}{\sigma_{\hat{\theta}}}\right)\,,
\end{aligned}
\label{eq:alphabeta_CI_norm}
\end{eqnarray}

where $\Phi$ is the cummulative distribution function of the standard Gaussian distribution described in Sec.~\ref{sec:norm}. One can determine the confidence interval from Eq.~\ref{eq:alphabeta_CI_norm} as

\begin{eqnarray}
\begin{aligned}
[a,\,b] &= [\hat{\theta}_{\text{obs}} - \sigma_{\hat{\theta}}\Phi^{-1}\left(1 - \alpha\right),\,\hat{\theta}_{\text{obs}} + \sigma_{\hat{\theta}}\Phi^{-1}\left(1 - \beta\right)]\,,
% b &= \hat{\theta}_{\text{obs}} + \sigma_{\hat{\theta}}\Phi^{-1}\left(1 - \beta\right)\,.
\end{aligned}
\label{eq:CI_norm}
\end{eqnarray}

which can be rewritten in terms of the $\alpha-$point $z$ introduced in Eq.~\ref{eq:zscore} as

\begin{eqnarray}
\begin{aligned}
[a,\,b] &= [\hat{\theta}_{\text{obs}} - \sigma_{\hat{\theta}}z_{1 - \alpha},\,\hat{\theta}_{\text{obs}} + \sigma_{\hat{\theta}}z_{1 - \alpha}]\,,
% b &= \hat{\theta}_{\text{obs}} + \sigma_{\hat{\theta}}\Phi^{-1}\left(1 - \beta\right)\,.
\end{aligned}
\label{eq:CI_norm_z}
\end{eqnarray}

One can see from Eq.~\ref{eq:CI_norm_z} that $z_{1 - \alpha} = \Phi^{-1}\left(1 - \alpha\right)$ represents the distance of the limits of the confidence interval from the observed value of estimator $\hat{\theta}_{\text{obs}}$ in units of the standard deviation $\sigma_{\hat{\theta}}$ given significance level $\alpha$. 

The values of the confidence level for the certain values of the $\Phi^{-1}$ and vice versa are shown in Tables~\ref{tab:integer_phi} and \ref{tab:cl_phi}.

\FloatBarrier

\subsection{Goodness-of-fit tests}
\label{subsec:gof}

\begin{figure}[!htb]
  \centering
  \includegraphics[width = 0.9\textwidth]{pval}
  \caption[Probability distribution functions for the test statistic under the assumption of the null hypothesis.]{Probability distribution functions for the test statistic under the assumption of the null hypothesis. Blue line and shaded area represent significance level $\alpha$. Red line and shaded area represent the observed value of test statistic and corresponding $p-$value.} 
  \label{fig:hypo_demo}
\end{figure}

Consider now a situation when one wants to test how well the null hypothesis agrees the observed data regardless of any specific alternaticve hypothesis. Such test then is referred to as \textit{goofness-of-fit} and requires a specific test statistic that represents the level of agreement between the null hypothesis (generally speaking prediction) and observed data. 

The result of the goodness-of-fit test is represented by so called \textit{$p-$value} or probability of obtaining the result as compatible or less with \hnull than what is actually observed under the assumption of \hnull. One can notice that the definition of the $p-$value is almost identical to the definition of the significance level $\alpha$ (see Section~\ref{sec:hypo_test}) and thus $p-$value is sometimes called the \textit{observed significance level} or \textit{confidence level} of the test. But unlike the significance level that is specified before the statistical test, the $p-$value is a random variable itself specified only after obtaining the measurement as shown on Fig.~\ref. It actually means that unlike $\alpha$ the $p-$value does not states the probability that \hnull is actually true.
Consider significance level $\alpha = 0.05$. If one measures $p-$value that is less than the significance level, $p < 0.05$, one claims that observed value is different from the expectation value at 95\% confidence level.

Since the definitions of the $p-$value and significance level are similar, one can use the same equations to calculate the $p-$value. Then according to Eq.~\ref{eq:alphabeta_CI_1}, $p-$value is defined as

\begin{eqnarray}
\begin{aligned}
z &= \frac{\hat{\theta}_{\text{obs}} - \hat{\theta}_{\text{exp}}}{\sigma_{\hat{\theta}}} \\
p &= 1 - \Phi\left(z\right)\,.
\end{aligned}
\label{eq:pval}
\end{eqnarray}

One can see from Eq.~\ref{eq:pval} that the goodness-of-fit test can be interpreted in terms of $z$ value, since the condition $p < \alpha$ is equivalent to $z > z_{1 - \alpha}$. This interpretation is referred to as \textit{$z-$test}. In the case described above, one had only one measurement of the variable $x$ under the assumption of \hnull only. Such $z-$test is then referred to as \textit{one sample}.

Consider a different experiment where one has two independent measurements of the same random variable $x$, baseline \xnull and alternative \xalt. Introduce the null hypothesis \hnull as $\xnull - \xalt = 0$ and alternative hypothesis \halt as $\xnull - \xalt \neq 0$. Then one can describe two measurements as a single measurement of the variable $\xnull - \xalt$. Let \snull and \salt be standard deviations of baseline and alternative measurement. According to Eq.~\ref{eq:var_func}, the p.d.f. of the random $\xnull - \xalt$ variable is described by the following standard deviation:

\begin{eqnarray}
\begin{aligned}
\sigma^{2}_{\xnull - \xalt} = \snull^{2} + \salt^{2}  
\end{aligned}
\label{eq:meanstd_two}
\end{eqnarray}

It means that the corresponding $z-$value for the $z-$test is 

\begin{eqnarray}
\begin{aligned}
z &= \frac{\xnull - \xalt - 0}{\sigma_{\xnull - \xalt}}\,.
\end{aligned}
\label{eq:ztest_two}
\end{eqnarray}

\FloatBarrier

\subsection{Pearson $\chi^2$ test}
\label{subsec:chi2test}

In Section~\ref{sec:chi2} we discussed the $\chi^2$ distribution and noted that in case of the sum of squares of independent variables described by the Gaussian distribution the following variable 

\begin{eqnarray}
	\chi^2 = \sum\limits_{i = 1}^{n} \frac{(x_i - \mu_i)^2}{\sigma_i^2}\,.
	\label{eq:chi2test_var}
\end{eqnarray}

follows the $\chi^2$ distribution with the number of degrees of freedom $n$. Consider one has observed values of $x_{i}$ and thecorresponding expected value of $\mu_i$ and variance $\sigma_i^2$. Then variable $\chi^2$ basically is a measure of the deviations between the observed and expected values in terms of standard deviations (or in general between the observed data and some hypothesis). The larger the value of $\chi^2$ is, the larger the discrepancy between observed data and the expected values. It means that $\chi^2$ is a test statistic that reflect the level of agreement between what was measured in the experiment and what was expected to be measured. This kind of the goodness-of-fit test is referred to as \textit{Pearson $\chi^2$ test}.

One can quantify the level of agreement between observations and expectation using same $p-$value definition but in terms of $\chi^2$ distributions as

\begin{eqnarray}
\begin{aligned}
p &= \int\limits_{\chi^2}^{\infty} f(x; n) dx = \int\limits_{\chi^2}^{\infty} \frac{1}{2^{n/2}\Gamma(n/2)} x^{n/2 - 1} e^{-x / 2} dx\,.
\end{aligned}
\label{eq:pval}
\end{eqnarray}

One can recall that the expectation value of the $\chi^2$ distribution is equal to $n$ (see Eq.~\ref{eq:muvar_chi2}). It means that one use $\chi^2/n$ as a measure of agreement between the observed data and hypothesis.

Consider now a multinomial experiment (see Section~\ref{subsec:binom}) with the total number of entries $n_{\text{tot}} = \sum\limits_{i = 1}^{n} n_i$, individual numbers of entries $n_i$ and associated probabilities $p_i$. One constructs then $\chi^2$ test statistic as

\begin{eqnarray}
	\chi^2 = \sum\limits_{i = 1}^{n} \frac{(n_i - p_i n_{\text{tot}})^2}{p_i n_{\text{tot}}}\,.
	\label{eq:chi2test_multinom}
\end{eqnarray}

Such test statistic follow the $\chi^2$ distribution with the $n - 1$ degrees of freedom. We will see later that such test is particularly important for the statistical analysis of the numbers of each product purchased.

\FloatBarrier

\subsection{Minimum sample size}
\label{subsec:mss}

Consider two experiments that consists of \nnull and \nalt single observations of the same random variable $x$ respectively. Each observation results in a single observed values of $x$, $(\xnull)_{i}$ and $(\xalt)_{i}$. The corresponding estimators for the mean, $\mu_0$ and $\mu_1$, are given by Eq.~\ref{eq:mean_est}. Let each observation has the same standard deviation $\sigma$. According to Eq.~\ref{eq:muvar_discr_eqprob_av}, the corresponding variance is then $V_0,\,V_1 = \sigma^2/\sqrt{\nnull},\,\sigma^2/\sqrt{\nalt}$. In the limit of large $n$, such experiments are described by the Gaussian distributions with the means $\mu_0$ and $\mu_1$ and standard deviation $\snull\,\salt = \sqrt{V_1}\,,\sqrt{V_2}$.

Consider now these two experiment as a single experiment that observes the values $(\xalt)_{i} - (\xnull)_{i}$. Such experiment is then described by the Gaussian with the mean $d = \mu_0 - \mu_1$ and standard deviation $\sigma = \sqrt{\snull^2 + \salt^2}$. Then o ne can formulate the null hypothesis \hnull as $d = 0$ and the alternative hypothesis as $d \neq 0$. Assume some significance $\alpha$ and power $1 - \beta$. One can calculate two-sided confidence interval for a given significance level and express $z$ value for the significance and power as

\begin{eqnarray}
\begin{aligned}
z_{\alpha/2}  &= \frac{\xcr - 0}{\sigma} \\
z_{1 - \beta} &= \frac{d - \xcr}{\sigma} \,,
\end{aligned}
\label{eq:zab_mss}
\end{eqnarray}

where $\xcr = \mu_0 + \sigma_{d}z_{\alpha/2}$ denotes the upper limit of the confidence level for the null hypothesis at $1 - \alpha$ confidence level. Thus one can relate $z_{\alpha/2}$ and $z_{1 - \beta}$ as follows

\begin{eqnarray}
\begin{aligned}
z_{1 - \beta} &= - z_{\alpha/2} + \frac{d}{\sigma} = - z_{\alpha/2} + \frac{d}{ \sqrt{\frac{\sigma^2_{0}}{n_{0}} + \frac{\sigma^2_{1}}{n_{1}}} }\,,
\end{aligned}
\label{eq:zab_rel_mss}
\end{eqnarray}

Consider the case when one has equal sample sizes for the two experiments, $n = n_0 = n_1$. Then one can express $n$ from Eq.~\ref{eq:zab_rel_mss} as

\begin{eqnarray}
\begin{aligned}
n &= \frac{2\sigma(z_{1 - \beta} + z_{\alpha/2})^2}{d^2}\,.
\end{aligned}
\label{eq:zab_rel_mss}
\end{eqnarray}

This is a particularly powerful relation. Generally speaking, it tells us \textit{what sample size is given specific significance, power, difference between means and the standard deviation}. 

Assume one wants to observe the difference d and standard deviation $\sigma$ at 95\% confidence level and 80\% power level. Then Eq.~\ref{eq:zab_rel_mss} basically tells us \textit{what sample size do we need to be able to observe a difference $d$ between two measurements at 95\% confidence level and 80\% power level}.

In case when a single observation is actually a Bernoulli trial with the probability of "success" $p$, Eq.~\ref{eq:zab_rel_mss} becomes

\begin{eqnarray}
\begin{aligned}
n &= \frac{(\sigma_{0}z_{\alpha/2} + \sigma_{1}z_{1 - \beta})^2}{(\Delta p)^2}\,, 
\end{aligned}
\label{eq:zab_rel_mss_binom}
\end{eqnarray}

where $\Delta p = \palt - \pnull$ is the difference between the "success" probabilities and the pooled variances \snull and \salt are given by

\begin{eqnarray}
\begin{aligned}
	\snull &= \sqrt{2 \pnull (1 - \pnull)} \\
    \salt  &= \sqrt{\pnull (1 - \pnull) + \palt (1 - \palt)}\,.
\end{aligned}
\label{eq:var_pooled_mss_binom}
\end{eqnarray}

\subsection{Mann-Whitney $U$ test}
\label{subsec:mw_test}

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{Utest_norm}
	\caption[Distribution of the Mann-Whitney test statistic $U$ for two sets of observations, $\{20,23,23\}$ and $\{23,23,34\}$.]{Distribution of the Mann-Whitney test statistic $U$ for two sets of observations, $\{20,23,23\}$ and $\{23,23,34\}$. The shaded area shows the corresponding observed $p-$value.}
	\label{fig:Utest_norm}
\end{figure}

\FloatBarrier

Consider two random variables $x$ and $y$ that have continuous c.d.f., $f$ and $g$. Call $x$ \textit{stochastically smaller} than $y$ if $f(z) > g(z)$ for every $z$. One wants to perform a statistical test of the null hypothesis, $f = g$, against the alternative hypothesis, $f \neq g$. Consider $n$ measurements $\{x_{i}\}$ of the variable $x$ and $m$ measurements $\{y_{i}\}$ of the variable $y$ be arranged in order. Introduce test statistic $U$ as a counter of the number of times when $y$ preceeds $x$ and $U_{\text{obs}}$ as the observed test statistic based on measurements $\{x_{i}\}$ and $\{y_{i}\}$. Then one can check goodness-of-fit (see Section~\ref{subsec:gof}) and calculate the corresponding observed value of $p_{\text{obs}}$. If then $p_{\text{obs}} \leq \alpha$, where $\alpha$ is a given significance level, then the null hypothesis is rejected at the condifidence level $1 - \alpha$.

Let us construct the stastics $U$. First merge observations $\{x_{i}\}$ and $\{y_{i}\}$ and assign numeric ranks with $1$ corresponding to the smallest value of the observation. Consider an example of an ordered set of observations $\{20,23,23,23,23,34\}$ and let odd observations correspond to $x$ variable and even to $y$. The corresponding set of unadjusted ranks then would be $\{1, 2, 3, 4, 5, 6\}$. But one can notice that observations contains subsets of equal values $23$. In this case one should use adjusted ranks, referred to as \textit{tied ranks}, where the rank of the equal values is assigned to the midpoint of the unadjusted rankings. In case of considered set, ranks 1 and 6 are assigned to the single observed values $20$ and $34$, while all value $23$ get the midpoint rank $(2+5)/2 = 3.5$. Thus the final ranking would be $\{1, 3.5, 3.5, 3.5, 3.5, 6\}$. The observed sum of the ranks for observations of $x$ would be $T_{x} = 1 + 3.5 + 3.5 = 8$ and for observations $y$ would be $T_{y} = 3.5 + 3.5 + 6 = 13$. Given $n_{x}$ and $n_{y}$ as number of observation, the test statistic $U$ for both sample takes a form

\begin{eqnarray}
\begin{aligned}
	U_{x} &= T_{x} - \frac{n_x(n_x + 1)}{2} = 8 - 6 = 2 \\
    U_{y} &= T_{y} - \frac{n_y(n_y + 1)}{2} = 13 - 6 = 7 \\
    U &= U_x + U_y = n_xn_y = 9 \,.
\end{aligned}
\label{eq:U_test}
\end{eqnarray}

In case of the large samples of observations, $U$ can be approximated by the normal distribution with the following mean and standard deviation

\begin{eqnarray}
\begin{aligned}
	\mu_U &= n_xn_y = 9 \\
    \sigma &= \sqrt{\frac{n_xn_y(n_x+n_y+1)}{12}} = \sqrt{\frac{6 \cdot 6 \cdot (6+6+1)}{12}} = \sqrt{39}\,.
\end{aligned}
\label{eq:U_test_norm_untied}
\end{eqnarray}

The standard deviation in Eq.~\ref{eq:U_test_norm_untied} is given for the untied ranks. For the tied ranks, the corrected standard deviation becomes

\begin{eqnarray}
\begin{aligned}
	\mu_U &= \frac{n_xn_y}{2} = 4.5 \\
    \sigma_{\text{tied}} &= \sqrt{\frac{n_xn_y}{12}\left( n + 1 - \sum\limits_{i = 1}^{k} \frac{t_i^3 - t_i}{n(n-1)} \right)} = \sqrt{\frac{6 \cdot 6}{12}\left( 12 + 1 - \frac{4^3 - 4}{12\cdot(12-1)} \right)} \approx \sqrt{37.64}\,,
\end{aligned}
\label{eq:U_test_std_tied}
\end{eqnarray}

where $n = n_x + n_y$ is the total number of observations, $t_i$ is the number of observations that share the same rank $i$, and $k$ is the total number of distinct ranks. Given the observed value of $U_{\text{obs}} = 9$, we can calculate $z-$value as

\begin{eqnarray}
\begin{aligned}
	z = \frac{U_{\text{obs}} - \mu_U}{\sigma_{\text{tied}}} = \frac{9 - 4.5}{\sqrt{37.64}} = 0.73\,,
\end{aligned}
\label{eq:U_test_std_tied}
\end{eqnarray}

which corresponds to the $p-$value of $0.23$ for the 95\% confidence level. It indicates that there is no statistically significant difference between given two sets of observations $x$ and $y$ at 95\% confidence level, as shown on Fig.~\ref{fig:Utest_norm}.




