\chapter{A/B statistical test}
\label{sec:abtest}

\graphicspath{{figs/abtest/}}

\section{Click-through rate}
\label{sec:ctr}

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{CTR_exp_demo.png}
	\caption[Real CTR experiment from the \href{https://yeahketo.com/}{https://yeahketo.com/}.]{Real CTR experiment from the \href{https://yeahketo.com/}{https://yeahketo.com/}.}
	\label{fig:CTR_exp_demo}
\end{figure}

The \textit{click-through rate (CTR)} is defined as a ratio of users who clicked specific link (i.e. registration link) to the total number of users who visited the website or in other words ratio of click-throughs to the total visits

\begin{eqnarray}
\text{CTR} = \frac{\text{number of click-throughs}}{\text{number of visits}}
\label{eq:ctr}
\end{eqnarray}

CTR is commonly used as a measure of the success of some online advertising campaign for a website in order to bring more people to the website. The advertising can include a wide range of new features from colour change to the total new look of the website or bonuses and discounts. 

From the mathematical point of view, a single click-through event can be classified as a Bernoulli trial with a "success" type of the outcome, while no-click event as the same trial but with a "failure" type of the outcome. If the total number of visitors $n$ visited a certain website and $k$ of which clicked on a specific link, such click-through experiment can be described by the binomial distribution (see Section~\ref{subsec:binom})

\begin{eqnarray}
	f(k ; n, p) = \frac{n!}{k!(n-k)!} p^{k} (1-p)^{n-k}, \qquad k = 0,...,n\,,
	\label{eq:binom}
\end{eqnarray}

where $p$ denotes CTR or \textit{conversion rate}. For a large number of visitors CTR can be approximated by the Gaussian distribution as described in Section~\ref{subsec:norm_approx_binom}\footnote{Already for $n \sim 100$ Gaussian gives a good approximation as shown on Fig.~\ref{fig:norm_approx_binom}.\label{note:CTR_norm_approx}} The mean and standard deviation of the Gaussian distribution is given bt Eq.~\ref{eq:norm_approx_binom}. It means that one can use Gaussian probability distribution function (p.d.f.) to describe the CTR in the A/B statistical model.

Consider such an experiment where a new feature to the the mobile checkout form was added. Fig~\ref{fig:CTR_exp_demo} shows the results of such experiment where $\nnull = 1367$ users visited and $k_{0} = 183$ users clicked on the specific link for the baseline version of the website and corresponding $\nalt = 1348$ users visited and $k_{1} = 196$ users clicked on registration link for the alternative version. The corresponding CTRs are $\pnull = 183/1367 = 0.1339$ and $\palt = 0.1454$. 

Let us formulate the null and alternative hypothesis for such experiment. The two hypotheses can be introduced in terms of the CTR (\textit{standard} or \textit{unpooled}) or in terms of the difference between the baseline and alternative CTRs (\textit{pooled}) as shown in Table~\ref{tab:CTR_h0_h1}.

\begin{table}[!htb]	
		\begin{center}
			\normalsize{
			\begin{tabular}{c|l|l}			
				\hline
				\hline
					Hypothesis & Standard & Pooled \\
				\hline
					$\hnull$ & $p = \pnull$ & $p = \palt \neq \pnull$ \\
					$\halt$ & $\Delta p = \palt - \pnull = 0$ & $\Delta p = \palt - \pnull \neq 0$ \\
				\hline
				\hline	
		\end{tabular}
		}
		\end{center}
		\caption[Construction of hypotheses for the click-through experiment.]{Construction of hypotheses for the click-through experiment.}
	\label{tab:CTR_h0_h1}
\end{table}

\FloatBarrier

\subsection{Standard or unpooled statistical test}

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.48\linewidth]{ABtest_binom}
	\includegraphics[width = 0.48\linewidth]{ABtest_norm}
	\caption[Binomial distribution for the click-throughs and corresponding Gaussian approximation for the CTR.]{Binomial distribution for the click-throughs (left) and corresponding Gaussian approximation (right) for the CTR for the baseline measurement (blue) with $\nnull = 1367$ and $p_{0} = 0.1339$ and alternative measurement with $\nalt = 1348$ and $\palt = 0.1454$.}
	\label{fig:CTR_pdf_unpooled}
\end{figure}

First consider the unpooled version of the test where hypotheses are formulated in terms of absolute values of the CTR. We can construct the Gaussian probability distribution functions under null and alternative hypotheses with the means and standard deviations defined in Eq.~\ref{eq:norm_approx_binom} as shown on Fig.~\ref{fig:CTR_pdf_unpooled}. We need to introduce a significance level $\alpha$ and probability $\beta$ levels under which we reject or accept the null hypothesis \hnull. The commonly used values are $\alpha = 0.05$ and $\beta = 0.2$ which correspond to the confidence level (CL) of 95\% and power level (PL) of 80\% respectively. Also 90\% and 99\% are widely used as confidence levels for the A/B testing.

According to Eq.~\ref{eq:CI_norm} and Table~\ref{tab:cl_phi}, at the 95\% CL the two-sided central confidence interval for the null hypothesis is
\begin{eqnarray}
\begin{aligned}
[a,\,b] &= [\pnull - \sigma_{\pnull}\Phi^{-1}\left(1 - \alpha/2\right),\,\pnull + \sigma_{\pnull}\Phi^{-1}\left(1 - \alpha/2\right)] = [\pnull - 1.96\sigma_{\pnull},\,\pnull + 1.96\sigma_{\pnull}] \\
 & = [0.1158\,,0.1519]\,.
% b &= \hat{\theta}_{\text{obs}} + \sigma_{\hat{\theta}}\Phi^{-1}\left(1 - \beta\right)\,.
\end{aligned}
\label{eq:CI_CTR}
\end{eqnarray}

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{ABtest_crd}
	\caption[Gaussian probability distribution functions for the CTR experiment under the null hypothesis with $\nnull = 1367$ and $p_{0} = 0.1339$ and alternative hypothesis with $\nalt = 1348$ and $\palt = 0.1454$.]{Gaussian probability distribution functions for the CTR experiment under the null hypothesis (blue) with $\nnull = 1367$ and $p_{0} = 0.1339$ and alternative hypothesis (red) with $\nalt = 1348$ and $\palt = 0.1454$. Blue shaded areas represent the significance level $\alpha$. The red shaded area represents the statistical power of the test.}
	\label{fig:CTR_pdf_unpooled_power}
\end{figure}

A two sample $z-$value and corresponding $p-$value (see Section~\ref{subsec:gof}) for the two CTR measuremets can be calculated using Eq.~\ref{eq:ztest_two} and \ref{eq:pval}

\begin{eqnarray}
\begin{aligned}
z &= \frac{p_{1} - p_{0} - 0}{\sqrt{\sigma^{2}_{p_{0}} + \sigma^{2}_{p_{1}}}} = \frac{0.1454 - 0.1339}{\sqrt{0.0092^{2} + 0.0096^{2}}} \approx 0.84 \\
p &= 1 - \Phi(z) = 1 - \Phi(0.84) \approx 0.2 \,.
\end{aligned}
\label{eq:ztest_two_CTR}
\end{eqnarray}

The result of the $p-$test tells us that we cannot distinguish between the null and alternative hypotheses at 95\% confidence level since $p > 0.05$. Additionally we can calculate the power or probability to reject \hnull if the \halt is true. Given Eq.~\ref{eq:stdnormcdf} and \ref{eq:alphabeta_CI_norm}, the power is defined as the probability to observe values of $\palt \geq b$ 
\begin{eqnarray}
\begin{aligned}
1 - \beta &= 1 - \Phi\left(\frac{0.1519 - 0.1454}{0.0096}\right) \approx 0.25\,.
\end{aligned}
\label{eq:alphabeta_CI_norm}
\end{eqnarray}

Obtained power is below the threshold level $0.8$ above which we reject \hnull thus we cannot distinguish against \halt as shown on Fig.~\ref{fig:CTR_pdf_unpooled_power}.

\FloatBarrier

\subsubsection{Minimum sample size derivation}
\label{subsec:mss_calc}

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{ABtest_min_sample_size}
	\caption[Sample sizes required to observe different values $\text{MDE} = (\palt - \pnull)/\pnull$ of the MDE in the alternative experiment for the significance $\alpha = 0.05$ and power $1 - \beta = 0.8$.]{Sample sizes required to observe different values $\text{MDE} = (\palt - \pnull)/\pnull$ of the MDE in the alternative experiment for the significance $\alpha = 0.05$ and power $1 - \beta = 0.8$. Relative Plot is shown in a logarithmic scale. The baseline conversion rate and $p_{0}$ and standard deviation $\sigma^{2}_{p_{0}}$ are assumed the same as in original experiment.}
	\label{fig:CTR_mss}
\end{figure}

Now calculate the minimum sample size using Eq.~\ref{eq:var_pooled_mss_binom} required to see statistically significant desired difference in CTR, referred to as \textit{minimum detectable effect} (MDE), $\text{MDE} = (\palt - \pnull)/\pnull$, assuming the following conditions
\begin{itemize}
	\item Same baseline conversion rate $p_{0}$ and standard deviation $\sigma^{2}_{p_{0}}$ as in original experiment.
	\item 95\% confidence level and 80\% power level. 
	\item Different \textit{relative} increases in CTR: 10\%, 20\%, 30\%,... 
\end{itemize}

The distribution of the minimum sample size given the MDE is shown on Fig.~\ref{fig:CTR_mss}. Since we know the sample size and CTR for each of the value of the MDE, we can reproduce the results of such experiment as if it were to be made. 

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp10}
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp20} \\
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp40}
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp60}\\
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp80}
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp100}\\
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp120}
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp140}\\
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp160}
	\includegraphics[width = 0.48\linewidth]{ABtest_crd_dp200}

	\caption[Gaussian probability distribution functions for the hypothetical CTR experiments under the null hypothesis and alternative hypothesis with $p_{0} = 0.1339$, $p_{1} = (1 + \text{MDE})p_{0}$ and calculated minimum sample sizes.]{Gaussian probability distribution functions for the hypothetical CTR experiments under the null hypothesis (blue) and alternative hypothesis (red) with $p_{0} = 0.1339$, $p_{1} = (1 + \text{MDE})p_{0}$ and calculated minimum sample sizes. Blue shaded areas represent the significance level $\alpha$. The red shaded area represents the statistical power of the test.}
	\label{fig:CTR_mss}
\end{figure}

\FloatBarrier

\section{Number of each product purchased}
\label{sec:numprod}

\begin{table}[!htb]	
		\begin{center}
			\footnotesize{
			\begin{tabular}{l|c|c|c}			
				\hline
				\hline
					Product & \multicolumn{3}{c}{Purchases} \\
				\cline{2-4}
					Product & Original & Alternative & Total \\
				\hline
					YHFOR-3  & 4  & 9  & 13\\
					YHGR-3   & 2  & 3  & 5 \\
					YHKTO-1  & 1  & 1  & 2 \\
					YHKTO-3  & 3  & 2  & 5 \\
					YHKTO-5  & 18 & 17 & 35 \\
				\hline
					Total & 28 & 32 & 60 \\
				\hline
				\hline	
		\end{tabular}
		}
		\end{center}
		\caption[Number of different products purchased on \href{https://yeahketo.com/}{https://yeahketo.com/} in the original and alternative experiments.]{Number of different products purchased on \href{https://yeahketo.com/}{https://yeahketo.com/} in the original and alternative experiments. YHFOR stands for Yeah! Forskolin, YHGR for Yeah! Caralluma and YHKTO for Yeah! Keto. Numbers means numbers of same items purchased in the pack.}
	\label{tab:numprod_purch}
\end{table}

Consider now that we would like to see if some change in the online system (new advertising, discount, new look of the website, etc.) changes the spectrum of the products purchased. We can consider such experiment as a set of different Bernoulli trials for each separate product with "success" outcome defined as a purchase of a specific product. Then the experiment can be discribed by the multinomial distribution function (see Eq.~\ref{eq:multinom}). Then according to Section~\ref{subsec:chi2test}, we can use a Pearson $\chi^2$ test to check if we have any difference in the distributions of products purchased in the original and alternative experiments. 

Consider an experiment shown in Table~\ref{tab:numprod_purch} where we have five different products (different packs considered as different products as well) purchased by the customers. Then the $\chi^2$ test statistics is given by Eq.~\ref{eq:chi2test_multinom}

\begin{eqnarray}
	\chi^2 = \sum\limits_{i = 1}^{n} \frac{(n_i - p_i n_{\text{tot}})^2}{p_i n_{\text{tot}}}\,.
	\label{eq:numprod_chi2}
\end{eqnarray}

Define $\chi^2$ for a specific product, e.g. YHFOR-3. First we need to calculate $p_i n_{\text{tot}}$ which is basically the number of products we \textit{expect to be purchased} in each variant given the total numbers presented in the last column of Table~\ref{tab:numprod_purch}. The total number of purchased YHFOR-3 product is 13, meaning that probability to purchase this specific item is $13/60$. Given that the total number of purchases in the original experiment is 28, we can estimate the expected number of purchases of YHFOR-3 in the original experiment as $28*13/60$. The corresponding number for the alternative experiment is then $32*13/60$. Then the $\chi^2$ associated with this specific product is

\begin{eqnarray}
	\chi_{YHFOR-3}^2 = \frac{(4 - 28*13/60)^2}{28*13/60} + \frac{(9 - 32*13/60)^2}{32*13/60} \approx 1.32\,.
	\label{eq:numprod_chi2_YHFOR}
\end{eqnarray}

Same procedure can be done for all products and the total $\chi^2$ is defined as

\begin{eqnarray}
	\chi^2 = \chi_{YHFOR-3}^2 + \chi_{YHGR-3}^2 + \chi_{YHKTO-1}^2 + \chi_{YHKTO-3}^2 + \chi_{YHKTO-5}^2 \approx 2.09 \,.
	\label{eq:numprod_chi2_total}
\end{eqnarray}

This $\chi^2$ then follows the $\chi^2$ distribution with the number of degrees of freedom $n = (n_{\text{products}} - 1)(n_{\text{variants}}) = 4$. Since we know the observed value $\chi^2 = 2.09$, we can know calculate the $p-$value for this $\chi^2$ test

\begin{eqnarray}
\begin{aligned}
p &= \int\limits_{2.09}^{\infty} \frac{1}{2^{4/2}\Gamma(4/2)} x^{4/2 - 1} e^{-x / 2} dx \approx 0.72 \,.
\end{aligned}
\label{eq:numprod_pval}
\end{eqnarray}

Assume the significance $\alpha = 0.05$ meaning that we want to reject or accept the null hypothesis at 95\% confidence level. The corresponding critical $p-$value $p_{\text{crit}}$ is 

\begin{eqnarray}
\begin{aligned}
p_{\text{crit}} &= \int\limits_{z_{1-\alpha/2}}^{\infty} \frac{1}{2^{4/2}\Gamma(4/2)} x^{4/2 - 1} e^{-x / 2} dx \approx 9.488 \,,
\end{aligned}
\label{eq:numprod_pval_crit}
\end{eqnarray}

illustrated on Fig.~\ref{fig:chi2multinom}. We can see that observed $p-$value is less than the critical value meaning that we cannot reject the null hypothesis or practically speaking claim that there is statistically significant change in the items purchased.

\begin{figure}[!htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{ABtest_chi2multinom}
	\caption[G$\chi^2$ distribution for the observed test statistics with the number of degrees of freedom $n = 4$.]{$\chi^2$ distribution for the observed test statistics for the number of degrees of freedom $n = 4$. Vertical line shows the observed value of $\chi^2$. The shaded area shows the corresponding observed $p-$value.}
	\label{fig:chi2multinom}
\end{figure}

\FloatBarrier

\section{Average revenue per user}
\label{sec:revenue}

The average revenue per user is a type of exception from all tests that we performed before. It can be hardly described by any meaningful probability distribution function, since most of the transactions are 0 meaning that users just visited website but never purchased anything. It means that we should use more tricky stastical test to analyze the difference in user spendings. We can utilize the Mann-Whitney $U$ rank test which is described in details in Section~\ref{subsec:mw_test}. Here we analyze not the average revenue per user but the value of each transaction including also zero transactions which correspond to users that visited website but purchased nothing. The values of transcations from each variation of the experiment are taken as independent observations and then the whole same procedure is followed as described in Section~\ref{subsec:mw_test} where one replaces the example sets of observation with a given values from the experiment. 


