#!/usr/bin/env python

__doc__ = """
submit_condor.py
"""

from optparse import OptionParser
from subprocess import Popen, PIPE
from tempfile import gettempdir, gettempprefix, NamedTemporaryFile
from glob import glob
from GroupFilesBySize import split_seq_size
import re, sys, os, datetime, random, string

def tmpNameTarball(path):
    """ tmpNameTarball(path) => tarballPath: create random filename for tarball in location @path"""
    return os.path.join(path, "%s%s.tar.lzma" % (gettempprefix(), ''.join(random.choice(string.letters) for i in xrange(6))))

def split_seq(seq, num_pieces):
    start = 0
    for i in xrange(num_pieces):
        stop = start + len(seq[i::num_pieces])
        yield seq[start:stop]
        start = stop

def exec_shell(cmd):
    """ exec_shell(cmd) => status, stdout, stderr: execute @cmd on a shell"""
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    retval = p.returncode
    return retval, out, err

def main():

    # fetch location of this script
    exec_path = os.path.abspath(os.path.dirname(sys.argv[0]))

    # configure optparse
    parser = OptionParser(usage="usage: %prog [options] INPUT")
    parser.add_option("--exec", dest="binary", help="binary/job options to execute")
    parser.add_option("--name", dest="name", help="job name")
    parser.add_option("--output", dest="output", help="directory to store output")
    parser.add_option("--asetup", dest="asetup", help="tags to setup current athena version")
    parser.add_option("--nGBmemory", dest="nGBmemory", type=int, default=4, help="GB of memory requested per job")
    parser.add_option("--nGBPerJob", dest="nGBPerJob", metavar="N", help="allocate max. N GB per subjob", type=float, default=None)
    parser.add_option("--tar", dest="tar", help="Specify tarball of Workarea to be used", default=None)
    parser.add_option("--priority", dest="priority", help="relative priority of the job", type=int, default=0)
    parser.add_option("--exclude", metavar="FILE", dest="excludeFrom", help="FILE containing patterns of files excluded from tarball generation. see `man tar` for details", default=None)
    parser.add_option("--no-input", dest="no_input", help="do not use input files", action="store_true",
                      default=False)

    # parse args
    options, args = parser.parse_args()

    # consistency checks
    if not args and not options.no_input:
        parser.print_help()
        raise RuntimeError("ERROR: Mandatory field INPUT is missing!")
    elif args:
        if all(map(lambda inp: not inp.endswith('.txt'), args)):
            inp_is_fileList=False
        elif len(args) == 1 and args[0].endswith('.txt'):
            inp_is_fileList=True
        else:
            raise RuntimeError("ERROR: INPUT only supports set of files and folders or a single text file!")
        
    if not options.binary:
        print "ERROR: Missing required argument '--exec'"
        parser.print_help()
        sys.exit(1)

    if not options.name:
        print "ERROR: Missing required argument '--name'"
        parser.print_help()
        sys.exit(1)

    if not options.output:
        print "ERROR: Missing required argument '--output'"
        parser.print_help()
        sys.exit(1)

    if not options.asetup:
        print "ERROR: Missing required argument '--asetup'"
        parser.print_help()
        sys.exit(1)

    if options.excludeFrom and not os.path.exists(options.excludeFrom):
        print "ERROR: argument of --exclude-from must exist"
        parser.print_help()
        sys.exit(1)

    path = os.path.join(options.output, options.name)

    if not os.path.isdir(path):
        print "WARNING: Output directory does not exists. Creating..."
        try:
            os.makedirs(path)
        except:
            print "ERROR: Creating output path: %s" % options.output
            sys.exit(1)

    # create logfile
    logfile = open(os.path.join(path, "submit.log"), "a+")
    logfile.write("Log %s\n" % datetime.datetime.today())

    # create or copy tarball
    if options.tar:
        print "INFO: using tarball provided by user ..."
        r, o, e = exec_shell("cp %s %s" % (options.tar, os.path.join(path, os.path.basename(options.tar))))
        tarball = os.path.join(path, os.path.basename(options.tar))
        if r != 0 or not os.path.exists(tarball):
            raise RuntimeError("ERROR: tarball copy failed, output was:\n%s\n%s" % (o, e))

    else:
        print "INFO: creating tarball ..."
        tarball = tmpNameTarball(path)

        # create file to be passed to --exclude-from option
        excludeFile = None
        excludeFileName = None
        if not options.excludeFrom:
            if not os.getenv('CMAKE_PREFIX_PATH',False):
                excludeFileName = os.path.join(exec_path, "exclude.txt")
            else:
                excludeFileName = os.path.join(exec_path, "exclude_cmake.txt")
        else:
            excludeFileName = options.excludeFrom
 
        # switch to $TestArea and build tarball
        basedir = os.getcwd()

        if not os.getenv('TestArea',False):
            raise RuntimeError("TestArea not defined, did you setup athena?")

        # TestArea should be in source, for tarball we also want build and run
        if basedir != os.environ['TestArea']:
            os.chdir(os.environ['TestArea']) 
        retval, out, err = exec_shell("tar -caf %s -X %s *" % (tarball, excludeFileName))
        if retval != 0 or not os.path.exists(tarball):
            raise RuntimeError("ERROR: tarball creation failed, output was:\n%s\n%s" % (out, err))

    # check size of tarball
    statinfo = os.stat(tarball)
    if statinfo.st_size >= (256L * 1024L * 1024L): # 256 MB
        print "TarBall %s" % tarball
        raise RuntimeError("TarBall is too big ( %d > %d ), use the excludeFile!"
                           % (statinfo.st_size, 256L * 1024L * 1024L))

    print "INFO: submitting jobs ..."

    # create filelist
    input_files = []
    endings1=map(lambda n: '.%d' % n, range(1,10))
    if inp_is_fileList:
        with open(args[0]) as f:
            split_lines = f.read().splitlines()
            for string in split_lines:
                input_files += string.split() 
    else:
        for inp in args:
            if not os.path.exists(inp):
                print "Path %s does not exists - skipping to next input" % inp
                continue
            if os.path.isdir(inp):
                input_files.extend([ os.path.join(inp, file) for file in os.listdir(inp) 
                                     if file[len(file)-2:] in endings1 or '.root' in file ])
            else:
                input_files.append(inp)

    # create split filelist 
    if not input_files and not options.no_input:
        print "ERROR: no input files found"
        sys.exit(1)
    elif input_files:
        nInputFiles = len(input_files)

        if options.nGBPerJob:
            split_files = split_seq_size(input_files, long(options.nGBPerJob * 1024. * 1024. * 1024.)) # byte to GB
            options.njobs = len(split_files)
            print "INFO: Using %d subjobs with max. %.1f GB" % (len(split_files), options.nGBPerJob)
        else:
            options.njobs = nInputFiles
            split_files = split_seq(input_files, options.njobs)
    else:
        split_files = [[]] * options.njobs # create list of njobs empty list

    # open file containing the file lists for each subjob
    fileList = open(path+"/fileList.txt","w")

    i = 0
    for files in split_files:
        if not files and not options.no_input: break

        # put together arguments for the job
        if files:
            condorArgs="bin='%s' filelist=%s copy=%s memory=%d tarball=%s asetup='%s' outputDir=%s nJobs=%s Name=%s" % (
                options.binary.replace('\"', '\\\"'), # escape double quotes
                path+"/fileList.txt",
                "true",
                options.nGBmemory,
                os.path.abspath(tarball),
                options.asetup.replace(',', ' '),
                path,
                options.njobs,
                options.name)
            fileList.write(' '.join(files)+'\n')
        else:
            condorArgs="bin='%s' copy=%s memory=%d tarball=%s asetup='%s' outputDir=%s nJobs=%s Name=%s" % (
                options.binary.replace('\"', '\\\"'), # escape double quotes
                "true",
                options.nGBmemory,
                os.path.abspath(tarball),
                options.asetup.replace(',', ' '),
                path,
                options.njobs,
                options.name)
        i = i + 1

    condor_cmd = "condor_submit %s $UTILITYDIR/submit/descr_athena.descr" % (condorArgs)
    cmd = "cd %s && %s" % (path, condor_cmd)

    # submit job
    retval, out, err = exec_shell(cmd)

    if retval != 0:
        print "ERROR: executing '%s' failed." % cmd
        print "ERROR:", out.strip()
        print "ERROR:", err.strip()
        sys.exit(1)

    logfile.write("%s : %s\n" % (out.strip(), condor_cmd))
    print out.strip()

    fileList.close()

    logfile.close()

if __name__ == "__main__":
    main()

