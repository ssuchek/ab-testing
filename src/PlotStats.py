import CalcStats as cs

import numpy as np
import seaborn as sb
#import pandas
import scipy.stats as sts
from matplotlib import pyplot as plt
from matplotlib import patches as mpatches

def plot_norm_distr(axis, mu = 0, std = 1, color = 'grey', label = None, isCR = True):
	""" Plots normal distribution given mean and standard deviation.
	"""
	x = np.linspace(mu - 7*std, mu + 7*std, 1000)
	y = sts.norm(mu, std)

	if isCR:
		axis.plot(x, y.pdf(x), color = color, label = r'(p, $\sigma_p$) = ({0:.2f}%, {1:.2f}%)'.format(100*mu, 100*std))
	else:
		axis.plot(x, y.pdf(x), color = color, label = r'(p, $\sigma_p$) = ({0:.0f}, {1:.0f})'.format(mu, std))

def plot_binom_distr(axis, n, p, color = 'grey', label = None):
	""" Plots binomial distribution given sample size and probability of success.
	"""
	x = np.linspace(0.75*n*p, 1.25*n*p, 2*n*p+1)
	y = sts.binom(n, p).pmf(x)

	axis.bar(x, y, alpha = 0.5, color = color, label = "(n, p) = ({0:.0f}, {1:.2f}%)".format(n, 100*p))
	#axis.plot(x, y, color = color, label = "(n, p) = ({0:.0f}, {1:.2f}%)".format(n, 100*p))

def plot_CI(axis, mu = 0, std = 1, n = 1, ybottom = 0, ytop = 1, color = 'grey', alpha = 0.05, one_tail = False):
	""" Plots confidence interval of the normal distribution for a given significance level
		and one/two tailed measurement.
	"""
	left, right = cs.confidence_interval(mu, std, n, alpha, one_tail = one_tail)

	print (left, right)

	y = sts.norm(mu, std)

	axis.axvline(x = left, color=color, linestyle='--', alpha=0.5)
	axis.axvline(x = right, color=color, linestyle='--', alpha=0.5)

def plot_pooled_statistical_power(axis, mu = 0, std = 1, n = 1, alpha = 0.05, stat_type = 'significance', one_tail = False):
	""" Plots the significance area of the null and alternative hypotheses in A/B test for a given significance level.
		Args:            
			alpha:  significance level or probability of rejecting the true null hypothesis (default value is 0.05 corresponds to 95% confidence level)
			power:  probability to reject the null hypothesis if alternative hypothesis is true for a given significance level.
					power = Pr(reject H0 | true H1) = PDF(x > r) = 1 - CDF(r)
					where r stands for the upper value of the corresponding confidence interval for H0.
			beta:   false negative rate or probability of not rejecting the false null hypothesis. Related to power as follows:
					power = 1 - beta. Default value is 0.2 which implies a 4-to-1 trade off between the type-II error (power) and
					type-I error (significance).
		Cross-checked:
			https://abtestguide.com/calc/
			https://www.surveymonkey.com/mp/ab-testing-significance-calculator/
		References:
			http://www.medicine.mcgill.ca/epidemiology/Joseph/publications/Methodological/binexact.pdf
			http://statweb.stanford.edu/~susan/courses/s141/hopower.pdf
			http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_Power/BS704_Power_print.html
			https://www.kaggle.com/tammyrotem/ab-tests-with-python
	"""
	left, right = cs.confidence_interval(0, std, n, alpha, one_tail = one_tail)

	x = np.linspace(mu - 7*std, mu + 7*std, 1000)
	y = sts.norm(mu, std)

	c = 'grey'

	area = 0

	if stat_type == 'significance':
		if one_tail:
			axis.fill_between(x, 0, y.pdf(x), color = '#1E90FF', alpha = '0.25', where = (x > right))
		else:
			cond = ((x < left) | (x > right))
			axis.fill_between(x, 0, y.pdf(x), color = '#1E90FF', alpha = '0.25', where = cond)
	elif stat_type == 'power':
		area = cs.get_pooled_power(mu, std, n, alpha, one_tail = one_tail)
		axis.text(0.65, 0.52, r'power = {0:.2f}%'.format(100*area), fontsize = 16, ha = 'left', va = 'center', transform = axis.transAxes)
		if one_tail:
			axis.fill_between(x, 0, y.pdf(x), color = '#FF6347', alpha = '0.25', where = (x > right))
		else:
			cond = ((x < left) | (x > right))
			axis.fill_between(x, 0, y.pdf(x), color = '#FF6347', alpha = '0.25', where = cond)
		print ("MDE = {0:.2f}%: power = {1:.3f}%".format(100*mu, 100*area))
	elif stat_type == 'beta':
		if one_tail:
			axis.fill_between(x, 0, y.pdf(x), color = '#DC143C', alpha = '0.25', where = (x < right))
		else:
			cond = ((x1 > left) & (x1 < right))
			axis.fill_between(x, 0, y.pdf(x), color = '#DC143C', alpha = '0.25', where = cond)
		area = y.cdf(right)
	
	#axis.text(4.5*std, .9*y.pdf(0), r'$\alpha$ = {0:.2f}'.format(area), fontsize=16, ha='right', color=c)

def plot_statistical_power(axis, mu0 = 0, std0 = 1, mu1 = 0, std1 = 1, n0 = 1, n1 = 1, alpha = 0.05, stat_type = 'significance', one_tail = False):
	""" Plots the significance area of the null and alternative hypotheses in A/B test for a given significance level.
		Args:            
			alpha:  significance level or probability of rejecting the true null hypothesis (default value is 0.05 corresponds to 95% confidence level)
			power:  probability to reject the null hypothesis if alternative hypothesis is true for a given significance level.
					power = Pr(reject H0 | true H1)
					Right tailed: power = PDF(x > r) = 1 - CDF(r)
					Two tailed:   power = PDF(x > r) + PDF(x < l) = 1 - CDF(r) + CDF(l)
					For instance, at CL 95% l and r are the lower and upper values of the corresponding confidence interval for null hypothesis.
			beta:   false negative rate or probability of not rejecting the false null hypothesis given significance level. Related to power as follows:
					power = 1 - beta(alpha). Default value a = 0.2 implies a 4-to-1 trade off between the type-II error (power) and
					type-I error (significance).
		Cross-checked:
			https://abtestguide.com/calc/
			https://www.surveymonkey.com/mp/ab-testing-significance-calculator/
		References:
			http://www.medicine.mcgill.ca/epidemiology/Joseph/publications/Methodological/binexact.pdf
			http://statweb.stanford.edu/~susan/courses/s141/hopower.pdf
			http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_Power/BS704_Power_print.html
			https://www.kaggle.com/tammyrotem/ab-tests-with-python
	"""
	left, right = cs.confidence_interval(mu0, std0, n0, alpha, one_tail = one_tail)

	x0 = np.linspace(mu0 - 7*std0, mu0 + 7*std0, 1000)
	y0 = sts.norm(mu0, std0)

	x1 = np.linspace(mu1 - 7*std1, mu1 + 7*std1, 1000)
	y1 = sts.norm(mu1, std1)

	c = 'grey'

	area = 0

	if stat_type == 'significance':
		if one_tail:
			axis.fill_between(x0, 0, y0.pdf(x0), color = '#1E90FF', alpha = '0.25', where = (x0 > right))
		else:
			cond = ((x0 < left) | (x0 > right))
			axis.fill_between(x0, 0, y0.pdf(x0), color = '#1E90FF', alpha = '0.25', where = cond)
	elif stat_type == 'power':
		area = cs.get_power(mu0, std0, mu1, std1, n0, n1, alpha = 0.05, one_tail = one_tail)
		axis.text(0.65, 0.52, r'power = {0:.2f}%'.format(100*area), fontsize = 16, ha = 'left', va = 'center', transform = axis.transAxes)
		if one_tail:	
			axis.fill_between(x1, 0, y1.pdf(x1), color = '#FF6347', alpha = '0.25', where = (x1 > right))
		else:
			cond = ((x1 < left) | (x1 > right))
			axis.fill_between(x1, 0, y1.pdf(x1), color = '#FF6347', alpha = '0.25', where = cond)
		print ("MDE = {0:.2f}%: power = {1:.3f}%".format(100*(mu1 - mu0), 100*area))
	elif stat_type == 'beta':
		if one_tail:	
			axis.fill_between(x1, 0, y1.pdf(x1), color = '#DC143C', alpha = '0.25', where = (x1 < right))
		else:
			cond = ((x1 > left) & (x1 < right))
			axis.fill_between(x1, 0, y1.pdf(x1), color = '#DC143C', alpha = '0.25', where = cond)
		area = y.cdf(right)

def plot_control_binom_distr(n0 = 1, n1 = 1, p0 = 1, p1 = 1, label = 'ABtest_binom', one_tail = False):
	""" Plots binomial distributions of two variants in the experiment and
		saves it under the name ABtest_binom.pdf
	"""

	fig, axis = plt.subplots()

	plot_binom_distr(axis, n0, p0, color = 'blue')
	plot_binom_distr(axis, n1, p1, color = 'red')

	# p_value = cs.p_val(n0, n1, p0, p1)
	# axis.text(0.85, 0.82, r'$p$ = {0}'.format(p_value), fontsize = 12, ha = 'left', va = 'center', transform = axis.transAxes)

	z_value = cs.z_val_binom(n0, n1, p0, p1)
	p_value = cs.p_val(z_value, one_tail = one_tail)
	# axis.text(0.85, 0.76, r'$p$ = {0:.4f}'.format(p_value), fontsize = 12, ha = 'left', va = 'center', transform = axis.transAxes)

	plt.xlabel('number of click-throughs')
	plt.ylabel('probability distribution function')
	#plt.show()

	plt.legend(loc='upper right', fontsize=16)
	#plt.legend.title()

	plt.savefig(label, bbox_inches='tight')

	plt.close(fig)

def plot_control_norm_distr(n0 = 1, n1 = 1, p0 = 1, p1 = 1, label = 'ABtest_norm'):
	""" Plots normal distributions of two variants in the experiment and
		saves it under the name ABtest_binom.pdf
	"""

	fig, axis = plt.subplots()

	sd0 = cs.norm_approx_binom_SD(n0, p0)
	sd1 = cs.norm_approx_binom_SD(n1, p1)

	plot_norm_distr(axis, p0, sd0, color = 'blue')
	plot_norm_distr(axis, p1, sd1, color = 'red')

	plt.xlabel('conversion rate')
	plt.ylabel('probability distribution function')
	
	plt.legend(loc='upper right', fontsize = 16)

	plt.savefig(label, bbox_inches='tight')

	plt.close(fig)

def plot_ABtest(n0 = 1, n1 = 1, p0 = 1, p1 = 1, alpha = 0.05, show_stat_type = False, stat_type = 'significance_power', 
	label = 'ABtest_crd', isSampleSizeTest = 'False', IsPooled = 'True', one_tail = False):
	""" Results of the A/B testing including:
		-- binomial and normal distributions of two variants in the experiment
		-- confidence interval of the null hypothesis for a given significance level
		-- statistical power and significance of the A/B testing
		-- minimum sample size prediction given significance level, statistical power, baseline conversion rate
			and minimum detectable effect

		Considers both pooled and unpooled testings:
		-- Unpooled:
			H0 hypothesis: p = p0 (baseline conversion rate)
			H1 hypothesis: p = p1 (measured conversion rate)
		-- Pooled:
			H0 hypothesis: dp = 0       (no change in conversion rate)
			H1 hypothesis: dp = p1 - p0 (nonzero change in conversion rate)
			Corresponding variance refers to as pooled variance given the variance and size of each population. 
			This estimate is used under the assumption that different populations have same variances even though
			means can differ. Provides more precise estimation comparing to the unpooled testing.
	"""

	fig, axis = plt.subplots()

	#fig, axis = plt.subplots()
	dp = p1 - p0
	
	if IsPooled:
		label = label + '_pooled'
		pooledSE = cs.pooled_SE(n0, n1, p0, p1)

		plot_norm_distr(axis, 0,  pooledSE, color = 'blue')

		ybottom, ytop = plt.ylim()
		plot_CI(axis, 0, pooledSE, 1, ybottom, ytop, color = 'blue', one_tail = one_tail)

		plot_norm_distr(axis, dp, pooledSE, color = 'red')

		left, right = cs.confidence_interval(0, pooledSE, n0, alpha, one_tail = one_tail)

		#prob = cs.prob_to_be_best(n0, n1, p0, p1, ntests = 100, alpha = 0.05)

		plt.xlabel('conversion rate difference')
		plt.ylabel('probability distribution function')
		#plt.show()

		if show_stat_type:
			if stat_type == 'significance':
				plot_pooled_statistical_power(axis, 0,  pooledSE, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)
			elif stat_type == 'power':
				plot_pooled_statistical_power(axis, dp, pooledSE, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)
			elif stat_type == 'significance_power':
				plot_pooled_statistical_power(axis, 0,  pooledSE, 1, alpha = 0.05, stat_type = 'significance', one_tail = one_tail)
				plot_pooled_statistical_power(axis, dp, pooledSE, 1, alpha = 0.05, stat_type = 'power', one_tail = one_tail)
			elif stat_type == 'beta':
				plot_pooled_statistical_power(axis, dp, pooledSE, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)

	else:
		sd0 = cs.norm_approx_binom_SD(n0, p0)
		sd1 = cs.norm_approx_binom_SD(n1, p1)

		z = (p1 - p0)/np.sqrt(sd0**2 + sd1**2)
		p = sts.norm().sf(abs(z))

		print ("z = {0}".format(z))
		print ("p = {0}".format(p))

		plot_norm_distr(axis, p0,  sd0, color = 'blue')

		ybottom, ytop = plt.ylim()
		plot_CI(axis, p0, sd0, 1, ybottom, ytop, color = 'blue', one_tail = one_tail)

		plot_norm_distr(axis, p1, sd1, color = 'red')

		plt.xlabel('conversion rate')
		plt.ylabel('probability distribution function')

		if show_stat_type:
			if stat_type == 'significance':
				plot_statistical_power(axis, p0, sd0, p1, sd1, 1, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)
			elif stat_type == 'power':
				plot_statistical_power(axis, p0, sd0, p1, sd1, 1, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)
			elif stat_type == 'significance_power':
				plot_statistical_power(axis, p0, sd0, p1, sd1, 1, 1, alpha = 0.05, stat_type = 'significance', one_tail = one_tail)
				plot_statistical_power(axis, p0, sd0, p1, sd1, 1, 1, alpha = 0.05, stat_type = 'power', one_tail = one_tail)
			elif stat_type == 'beta':
				plot_statistical_power(axis, p0, sd0, p1, sd1, 1, 1, alpha = 0.05, stat_type = stat_type, one_tail = one_tail)

	plt.legend(loc='upper right', fontsize = 16)

	if isSampleSizeTest:
		axis.text(0.65, 0.70, r'$\alpha$ = 0.05', fontsize = 16, ha = 'left', va = 'center', transform = axis.transAxes)
		axis.text(0.65, 0.64, r'$\beta$ = 0.2', fontsize = 16, ha = 'left', va = 'center', transform = axis.transAxes)
		axis.text(0.65, 0.58, r'MDE = {0:.0f}%'.format(100*dp/p0), fontsize = 16, ha = 'left', va = 'center', transform = axis.transAxes)
		#axis.text(0.85, 0.64, r'n = {0:.0f}'.format(n0), fontsize = 12, ha = 'left', va = 'center', transform = axis.transAxes)

	plt.savefig(label, bbox_inches='tight')

	plt.close(fig)

def plot_min_sample_size(p0, beta = 0.2, alpha = 0.05, label = 'ABtest_min_sample_size'):
	""" Plots the dependence of the minimum sample size on the minimum detectable effect. 
	"""
	x_min_size_list = []
	y_min_size_list = []
	min_sample_size = dict()

	for min_dp in np.arange(0.1, 2.05, 0.1):

		min_nexp = cs.min_sample_size(p0, min_dp, beta = beta, alpha = alpha)

		x_min_size_list.append(min_dp)

		y_min_size_list.append(min_nexp)

		min_sample_size.update( {min_dp : min_nexp} )

	fig, axis = plt.subplots()

	x = x_min_size_list
	y = y_min_size_list

	#axis.bar(x, y, alpha = 0.25)
	plt.plot(x, y, 'ro')
	plt.xlabel(r'MDE = $(p_1 - p_0)/p_0$')
	plt.ylabel('ln(minimum sample size)')

	axis.set_yscale('log')

	plt.savefig(label, bbox_inches='tight')

	plt.close(fig)

	return min_sample_size

def plot_Mann_Whitney_ranks(x = [], y = [], label = 'ABtest_MannWhitney_revenue'):

	u_mw, mu_mw, std_mw, p_mw = cs.mannwhitneyu_mod(x, y)

	fig, axis = plt.subplots()

	x = np.linspace(mu_mw - 7*std_mw, mu_mw + 7*std_mw, 1000)
	y = sts.norm(mu_mw, std_mw)

	print (u_mw)

	plot_norm_distr(axis, mu_mw, std_mw, color = 'blue', isCR = False)

	ybottom, ytop = plt.ylim()

	ymax = y.pdf(u_mw)/ytop
	
	axis.axvline(x = u_mw, ymin = 0, ymax = 1, color = 'blue', linestyle = '--', alpha = 0.5)
	#axis.fill_between(x, 0, y.pdf(x), color = 'blue', alpha = '0.25', where = (x > u_mw))
	#axis.fill_between(x, 0, y.pdf(x), color = 'blue', alpha = '0.25', where = (x > u_mw))
	axis.text(0.65, 0.88, r'p = {0}'.format(p_mw), fontsize = 12, ha = 'left', va = 'center', transform = axis.transAxes)

	plt.xlabel('Matt-Whitney rank test statistics')
	plt.ylabel('probability distribution function')

	# plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
	# plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
	
	plt.legend(loc='upper right')

	plt.savefig(label, bbox_inches='tight')

	plt.close(fig)

def plot_multinomial_chi2(x, alpha = 0.05, label = 'ABtest_chi2multinom', one_tail = 'False'):

	chi2_ndof = (len(x) - 1) * (len(x[0]) - 1)

	nvariants = len(x)

	ntot = np.sum(x)

	chisq = []
	ntot_variant = []

	for ivar in range(nvariants):
		ntot_var = np.sum(x[ivar])
		ntot_variant.append(ntot_var)

		chisq_var = []
		for jvar in range (len(x[ivar])):
			sum_over_product = np.sum(x[k][jvar] for k in range(nvariants))
			chisq_local = ( x[ivar][jvar] -  sum_over_product*ntot_var/ntot)**2 / (sum_over_product*ntot_var/ntot)
			chisq_var.append(chisq_local)

		chisq = np.append(chisq, chisq_var)

	chi2_total = np.sum(chisq)

	fig, axis = plt.subplots()

	xchi = np.linspace(sts.chi2.ppf(0.0000000000001, chi2_ndof), sts.chi2.ppf(0.999, chi2_ndof), 1000)

	ychi = sts.chi2(chi2_ndof)

	chi2_pval = 1 - ychi.cdf(chi2_total)

	plt.plot(xchi, ychi.pdf(xchi), color = 'blue')

	plt.xlabel(r'$\chi^2 = \sum_{i = 1}^{n} \frac{(n_i - p_i n)^2}{p_{i}n}$')
	plt.ylabel(r'$\chi^2$ distribution function')

	ybottom, ytop = plt.ylim()

	chi2_zval_crit = cs.z_val_chi2(alpha = alpha, ndof = chi2_ndof, one_tail = one_tail)
	chi2_pval_crit = cs.z_val_chi2(alpha = alpha, ndof = chi2_ndof, one_tail = one_tail)

	axis.axvline(x = chi2_total, ymin = 0, ymax = ychi.pdf(chi2_total)/ytop, color = 'blue', linestyle = '--', alpha = 0.5)
	axis.fill_between(xchi, 0, ychi.pdf(xchi), color = 'blue', alpha = '0.25', where = (xchi > chi2_total))
	axis.text(0.65, 0.80, r'ndof = {0}'.format(chi2_ndof), fontsize = 20, ha = 'left', va = 'center', transform = axis.transAxes)
	axis.text(0.65, 0.70, r'$\chi^2$ = {0}'.format(cs.nonzero_round(chi2_total)), fontsize = 20, ha = 'left', va = 'center', transform = axis.transAxes)
	axis.text(0.65, 0.60, r'$p$ = {0} < {1}'.format(cs.nonzero_round(chi2_pval), cs.nonzero_round(chi2_pval_crit)), fontsize = 20, ha = 'left', va = 'center', transform = axis.transAxes)

	plt.savefig(label, bbox_inches = 'tight')

	plt.close(fig)

	return chi2_total, chi2_pval, chi2_ndof



# def plot_pseudo_exp():

# 	keto_price = [0, 69, 147, 195]
# 	cara_price = [0, 65, 144]
# 	fors_price = [0, 67, 147]

# 	random_price_list = [random.choice(keto_price) + random.choice(cara_price) + random.choice(fors_price) for i in range(0,10000)]

# 	plt.plot