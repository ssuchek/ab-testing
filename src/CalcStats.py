import warnings

import numpy as np
import random as rn
import scipy.stats as sts

def nonzero_round(x):
    dx = str(x - int(x))
    for i, e in enumerate(dx[2:]):
        if e != '0':
            return int(x) + float(dx[:i+5])

def binom_SD(n, p):
    """ Calculates the standard deviation of the binomial distribution
    """
    sd = np.sqrt( n * abs(p) * (1 - abs(p)) )
    return sd

def norm_approx_binom_SD(n, p):
    """ Calculates the standard deviation of the normal approximation to the binomial distribution with mean = p (success rate) and std^2 = p*(1-p) / n
        References:
            http://statweb.stanford.edu/~susan/courses/s141/hoci.pdf
    """
    sd = np.sqrt( abs(p) * (1 - abs(p)) / n )
    return sd

def pooled_SE(n0, n1, p0, p1):
    """ Calculates the pooled variance for two measurements under assumption of equal variances of each measurement (mean can differ)
        References:
            https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1473027/
            https://wolfweb.unr.edu/~ldyer/classes/396/PSE.pdf
    """
    s0 = norm_approx_binom_SD( n0, p0 )
    s1 = norm_approx_binom_SD( n1, p1 )
    SE = np.sqrt( ( (n0 - 1)*s0**2 + (n1 - 1)*s1**2 ) / (n0 + n1 - 2)  )
    return SE

def stand_dev_null_alt(p0, dp):
    """ Calculates the pooled variances for the null and alternative hypotheses.
        Used in the minimum sample size calculation.
    """
    s0 = np.sqrt(2 * p0 * (1 - p0))
    s1 = np.sqrt(p0 * (1 - p0) + (p0 + dp) * (1 - (p0 + dp)))
    return s0, s1

def z_val_chi2(alpha=0.05, ndof=1, one_tail=False):
    """ Calculates Z score or the fractional number of standard deviations by which the actual observed value differs from the mean 
        observed or measured for a given significance level. 
        Z = (x_obs - mean)/s, where s is the standard deviation
        Args:
            one_tail = True:  left or right-tailed distribution, Z = F(1 - alpha)^{-1}
            one_tail = False: two-tailed distribution, Z = F(1 - alpha/2)^{-1}
    """
    if one_tail:
        a = 1 - alpha
    else:
        a = 1 - alpha/2

    z = sts.chi2(ndof).ppf(a)

    return z 

def p_val_chi2(z=0, ndof=1, one_tail=False):
    """ Calculates Z score or the fractional number of standard deviations by which the actual observed value differs from the mean 
        observed or measured for a given significance level. 
        Z = (x_obs - mean)/s, where s is the standard deviation
        Args:
            one_tail = True:  left or right-tailed distribution, Z = F(1 - alpha)^{-1}
            one_tail = False: two-tailed distribution, Z = F(1 - alpha/2)^{-1}
    """
    if one_tail:
        p =  2 * sts.chi2(ndof).sf(abs(z))
    else:
        p = sts.chi2(ndof).sf(z)

    return p

def z_val(alpha=0.05, one_tail=False):
    """ Calculates Z score or the fractional number of standard deviations by which the actual observed value differs from the mean 
        observed or measured for a given significance level. 
        Z = (x_obs - mean)/s, where s is the standard deviation
        Args:
            one_tail = True:  left or right-tailed distribution, Z = F(1 - alpha)^{-1}
            one_tail = False: two-tailed distribution, Z = F(1 - alpha/2)^{-1}
    """
    if one_tail:
        a = 1 - alpha
    else:
        a = 1 - alpha/2

    z = sts.norm().ppf(a)

    return z 

def p_val(z=0, one_tail=False):
    """ Calculates Z score or the fractional number of standard deviations by which the actual observed value differs from the mean 
        observed or measured for a given significance level. 
        Z = (x_obs - mean)/s, where s is the standard deviation
        Args:
            one_tail = True:  left or right-tailed distribution, Z = F(1 - alpha)^{-1}
            one_tail = False: two-tailed distribution, Z = F(1 - alpha/2)^{-1}
    """
    if one_tail:
        p =  2 * sts.norm().sf(abs(z))
    else:
        p = sts.norm().sf(z)

    return p

def z_val_binom(n0 = 1, n1 = 1, p0 = 1, p1 = 1):
    """ Z-test for the two binomial measurements
    """
    pooled_p = (n0 * p0 + n1 * p1)/(n0 + n1)

    z = (p1 - p0)/np.sqrt(pooled_p * (1 - pooled_p) * (1/n0 + 1/n1))

    return z

def confidence_interval(mu = 0, std = 1, size = 1, alpha = 0.05, one_tail = False):
    """ Calculates the confidence interval (CI) or the interval that might contain the true value of the unknown parameter for a given significance level.
        CI is associated with the confidence level (CL) which stands for the portion of the CIs that actually contain the true value.
        NOTE that CL DOES NOT represent the probability of the CI to contain the true value.  

        CI = [mean - Z(alpha)*std, mean + Z(alpha)*std] 
    """
    z = z_val(alpha, one_tail = one_tail)
    ci_left  = mu - z * std / np.sqrt(size)
    ci_right = mu + z * std / np.sqrt(size)

    return (ci_left, ci_right)

def get_pooled_power(mu = 0, std = 1, size = 1, alpha = 0.05, one_tail = False):
    """ Calculates the power or probability to reject the null hypothesis if alternative hypothesis is true for a given significance level.
        power = Pr(reject H0 | true H1)
        Right tailed: power = PDF(x > r) = 1 - CDF(r)
        Two tailed:   power = PDF(x > r) + PDF(x < l) = 1 - CDF(r) + CDF(l)
        For instance, at CL 95% l and r are the lower and upper values of the corresponding confidence interval for null hypothesis.
    """
    left, right = confidence_interval(0, std, size, alpha, one_tail = one_tail)

    x = np.linspace(mu - 10*std, mu + 10*std, 1000)
    y = sts.norm(0, 1)

    if one_tail:
        power = y.sf( (right - mu)/std )
    else:
        power = y.sf( (right - mu)/std )

    return power

def get_power(mu0 = 0, std0 = 1, mu1 = 0, std1 = 1, n0 = 1, n1 = 0, alpha = 0.05, one_tail = False):
    """ Calculates the power or probability to reject the null hypothesis if alternative hypothesis is true for a given significance level.
        power = Pr(reject H0 | true H1)
        Right tailed: power = PDF(x > r) = 1 - CDF(r)
        Two tailed:   power = PDF(x > r) + PDF(x < l) = 1 - CDF(r) + CDF(l)
        For instance, at CL 95% l and r are the lower and upper values of the corresponding confidence interval for null hypothesis.

    """
    left, right = confidence_interval(mu0, std0, n1, alpha, one_tail = one_tail)

    x = np.linspace(mu1 - 10*std1, mu1 + 10*std1, 1000)
    #y = sts.norm(mu1, std1)
    y = sts.norm(0, 1)

    if one_tail:
        power = y.sf( (right - mu1) / std1 )
        #power = y.sf(right)
    else:
        #power = y.sf(right) + y.cdf(left)
        power = y.sf( (right - mu1) / std1 )
        #power = y.sf( (right - mu1)/ std1 ) + y.cdf( (mu1 - left) / std1 )

    return power

def get_significance(mu = 0, std = 1, size = 1, alpha = 0.05, one_tail = False):
    """ Calculates significance for a given significance level.
    """
    left, right = confidence_interval(0, std, size, alpha, one_tail = one_tail)

    x = np.linspace(mu - 7*std, mu + 7*std, 1000)
    y = sts.norm(mu, std)

    if one_tail:
        significance = 1 - y.cdf(right)
    else:
        significance = 1 - y.cdf(right) + y.cdf(left)

    return significance


def min_sample_size(p0 = 1, mde = 0.1, beta = 0.2, alpha = 0.05):
    """ Calculates the minimum sample size for each measurement to perform A/B test for a given significance level.
        It utilizes knowledge from previous studies about the base conversion rate.
        Args:
            p0:     baseline conversion rate (bcr) from previous base measurement
            mde:    minimum detectable effect (mde) or miminimum re;ative required change in measured conversion rate under the alternative hypothesis. 
                    For instance, 0.1 means 10% relative increase in the bcr. 
            alpha:  significance level or probability of rejecting the true null hypothesis (default value is 0.05 corresponds to 95% confidence level)
            beta:   false negative rate or probability of not rejecting the false null hypothesis given significance level. Related to power as follows:
                    power = 1 - beta(alpha). Default value a = 0.2 implies a 4-to-1 trade off between the type-II error (power) and
                    type-I error (significance).
        Definitions:
            pooled_prob: pooled probability
                pooled_SE   = sqrt(pooled_prob*(1 - pooled_prob)*(1/n0 + 1/n1))
                pooled_prob = (k0 + k1)/(n0 + n1) = /k1 = (1 + a*k0), n0 = n1/ = 1/2 * (p0 + (1 + a)*p0)
            minimum sample size:
                    n1 = (Z(1 - alpha)*s1 + Z(1 - beta) * s2)^2 / dp^2
                    s0^2 = variance0^2 + variance0^2 = p0 * (1 - p0) + p0 * (1 - p0)
                    s1^2 = variance0^2 + variance1^2 = p0 * (1 - p0) + p1 * (1 - p1) = p0 * (1 - p0) + (p0 + dp) * (1 - (p0 + dp))
                Both calculations are comparable with n1 giving larger lower estimate.
        Cross-checks:
            https://www.abtasty.com/sample-size-calculator/
            https://www.optimizely.com/sample-size-calculator/
        References:
            http://www.medicine.mcgill.ca/epidemiology/Joseph/publications/Methodological/binexact.pdf
            http://statweb.stanford.edu/~susan/courses/s141/hopower.pdf
            http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_Power/BS704_Power_print.html
            https://www.kaggle.com/tammyrotem/ab-tests-with-python
    """

    z_alpha = z_val(alpha)
    z_beta  = z_val(beta, one_tail = True)

    #pooled_prob = (p0 + p0 * (1 + mde))/2

    dp = mde*p0

    #n = 2 * pooled_prob * (1 - pooled_prob) * (z_alpha + z_beta)**2 / (dp**2)

    sd0, sd1 = stand_dev_null_alt(p0, dp)

    n = (z_alpha * sd0 + z_beta * sd1)**2 / (dp**2)

    print ("BCR = {0:.2f}%, MDE = {1:.0f}%: n = {2:.0f}".format(100*p0, 100*mde, n))

    return np.ceil(n)

def mannwhitneyu_mod(x, y, use_continuity=True, alternative=None):
    """
    Standard Mann-Whitney rank test on samples x and y with modified output.
    Output:
        u: total Mann-Whitney rank test statistics of two samples
        (mu, s): the Mann-Whitney mean rank and the corresponding standard deviation assuming an asymptotically normally distributed test statistics U.
        pvalue: p-value assuming an asymptotic normal distribution of the test statistics U
    References
        https://en.wikipedia.org/wiki/Mann-Whitney_U_test
        H.B. Mann and D.R. Whitney, "On a Test of Whether one of Two Random
            Variables is Stochastically Larger than the Other," The Annals of
            Mathematical Statistics, vol. 18, no. 1, pp. 50-60, 1947.
    """
    if alternative is None:
        warnings.warn("Calling `mannwhitneyu` without specifying "
                      "`alternative` is deprecated.", DeprecationWarning)

    x = np.asarray(x)
    y = np.asarray(y)
    n1 = len(x)
    n2 = len(y)
    ranked = sts.rankdata(np.concatenate((x, y)))
    rankx = ranked[0:n1]  # get the x-ranks
    u1 = n1*n2 + (n1*(n1+1))/2.0 - np.sum(rankx, axis=0)  # calc U for x
    u2 = n1*n2 - u1  # remainder is U for y
    T = sts.tiecorrect(ranked)
    if T == 0:
        raise ValueError('All numbers are identical in mannwhitneyu')
    sd = np.sqrt(T * n1 * n2 * (n1+n2+1) / 12.0)

    meanrank = n1*n2/2.0 + 0.5 * use_continuity
    if alternative is None or alternative == 'two-sided':
        bigu = max(u1, u2)
    elif alternative == 'less':
        bigu = u1
    elif alternative == 'greater':
        bigu = u2
    else:
        raise ValueError("alternative should be None, 'less', 'greater' "
                         "or 'two-sided'")

    z = (bigu - meanrank) / sd
    if alternative is None:
        # This behavior, equal to half the size of the two-sided
        # p-value, is deprecated.
        p = sts.norm().sf(abs(z))
    elif alternative == 'two-sided':
        p = 2 * sts.norm().sf(abs(z))
    else:
        p = sts.norm().sf(z)

    u = u2
    # This behavior is deprecated.
    if alternative is None:
        u = min(u1, u2)
    return (u, meanrank, sd, p)


# def prob_to_be_best(n0 = 1, n1 = 1, p0 = 1, p1 = 1, ntests = 100, alpha = 0.05):

#     pooledSE = pooled_SE(n0, n1, p0, p1)

#     dp = p1 - p0

#     sd0 = norm_approx_binom_SD(n0, p0)
#     sd1 = norm_approx_binom_SD(n1, p1)

#     left0, right0 = confidence_interval(p0, sd0, 1, alpha)
#     left1, right1 = confidence_interval(p1, sd1, 1, alpha)

#     print (left0, right0)

#     power = get_pooled_power(dp, pooledSE, 1, alpha, one_tail = False)

#     #power = p0/p1 - 1

#     power_tests = []
    
#     for isample in range(1, ntests):

#         rp0 = rn.uniform(left0, right0)

#         #rp0 = p0

#         #rp1 = rn.uniform(left1, right1)

#         rp1 = p1

#         rdp = rp1 - rp0

#         # if isample % 10 == 0:
#         #     print (p0, dp, rp0, rdp)

#         pooledRSE = pooled_SE(n0, n1, rp0, rp1)

#         rpower = get_pooled_power(rdp, pooledRSE, 1, alpha, one_tail = False)

#         #rpower = rp0/rp1 - 1

#         power_tests.append(rpower)

#     #print (power_tests)

#     n_bigger = sum(val > power for val in power_tests)

#     prob = n_bigger / len(power_tests)

#     print ("Probability to be best: {0:.2f}".format(100 * prob))

#     return prob
