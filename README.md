# abtesting_statistics
Python based package to perform statistical A/B test with full documentation about main statistical tools needed and real examples using experimental data from [yeahketo](https://yeahketo.com/).

To run package use

>**source make.sh**

Usage:
> **python ABtest.py --bcon=462 --bnum=2208 --acon=463 --anum=2199 OPTIONAL[--isPooled --oneTail]**

Options:\
  **-h, --help**&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; show this help message and exit \
  **--bcon=BCON**&nbsp; &nbsp; &nbsp; &nbsp; baseline number of conversions\
  **--bnum=BNUM**&nbsp; &nbsp; &nbsp; size of the baseline sample\
  **--acon=ACON**&nbsp; &nbsp; &nbsp; &nbsp; alternative number of conversions\
  **--anum=ANUM**&nbsp; &nbsp; &nbsp; size of alternative sample\
  **--isPooled**&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; pooled A/B testing\
  **--oneTail**&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; one or two tailed A/B testing 
  
All statistical quantities are derived in [**CalcStats.py**](src/CalcStats.py).
All plotting tools are described in [**PlotStats.py**](src/PlotStats.py).
  
Package contains: 

1. Click-through rate test for two samples based on the Gaussian approximation of the binomial distribution and statistical significance and power test.
2. Minimum sample size estimation for a given minimum detectable effect, significance level and power.
3. Pearson chi-square statistical test for the number of each products purchased.
4. Matt-Whitney rank test for the revenue analysis.

Full description of the statistical tools, methods, distribution and practical usage based on the [real experiment from yeahketo](https://fab.yeahketo.com/stats/list/18) are given in [ABtest.pdf](docs/ABtest.pdf).
