import ROOT
from array import array
import sys
from collections import OrderedDict
import StringIO
import os
import itertools as it

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        if last == "":
            return s[start:]
        else:
            return s[start:end]
    except ValueError:
        return ""

def main():

    signalsZhad = dict()
    metBins = dict()
    numerator = {}
    denominator = {}

    particleName = "Z"

    fname = "/data/barn01/ssuchek/software/gitlab/MIL_monoV_v28092017/FrameworkSub_monoVH/data/XSections_13TeV.txt"

    if (particleName == "Z"):
        range_mcnumbers = range(303896, 303916)
    elif (particleName == "W"):
        range_mcnumbers = range(303916, 303938)
    else:
        print "Wrong range of MC channels!"

    with open(fname) as f:
        split_lines = f.read().splitlines()
        for line in split_lines:
            for mcnumber in range_mcnumbers:
                if str(mcnumber) in line:
                    print mcnumber
                    cols = line.split()         
                    signalsZhad["%s_" % (cols[4])] = (find_between(cols[4], "DM", "MM"), find_between(cols[4], "MM", ""), cols[1])

    # #signalsZhad["dmVZhadDM1MM10_"] = ("1","10","118.45")	
    # signalsZhad["dmVZhadDM1MM100_"]  = ("1","100","4.7855")
    # signalsZhad["dmVZhadDM1MM300_"] = ("1","300","0.6474")
    # #signalsZhad["dmVZhadDM10MM10_"]  = ("10","10","1.2353")
    # signalsZhad["dmVZhadDM10MM100_"]  = ("10","100","4.781")
    # signalsZhad["dmVZhadDM10MM10000_"] = ("10","10000","0.00016593")
    # signalsZhad["dmVZhadDM50MM10_"]  = ("50","10","0.071823")
    # signalsZhad["dmVZhadDM50MM95_"]  = ("50","95","0.4039")
    # signalsZhad["dmVZhadDM50MM300_"] = ("50","300","0.64358")
    # signalsZhad["dmVZhadDM150MM10_"]  = ("150","10","0.0068938")
    # signalsZhad["dmVZhadDM150MM295_"]  = ("150","295","0.095625")
    # signalsZhad["dmVZhadDM150MM1000_"]  = ("150","1000","0.024815")    
    # signalsZhad["dmVZhadDM500MM10_"]  = ("500","10","0.00016593")
    # signalsZhad["dmVZhadDM500MM995_"]  = ("500","995","0.0052253")
    # signalsZhad["dmVZhadDM500MM2000_"]  = ("500","2000","0.0015388")
    # signalsZhad["dmVZhadDM1000MM10_"]  = ("1000","10","0.0000064578")
    # signalsZhad["dmVZhadDM1000MM1995_"] = ("1000", "1995","0.00032993")

    metBins["150_200ptv"] = ("150_200trptv", "150_200ptv__150_200trptv")
    metBins["200_250ptv"] = ("200_250trptv", "200_250ptv__200_250trptv")
    metBins["250_300ptv"] = ("250_300trptv", "250_300ptv__250_300trptv")
    metBins["300_350ptv"] = ("300_350trptv", "300_350ptv__300_350trptv")
    metBins["350_400ptv"] = ("350_400trptv", "350_400ptv__350_400trptv")
    metBins["400_450ptv"] = ("400_450trptv", "400_450ptv__400_450trptv")
    metBins["450_500ptv"] = ("450_500trptv", "450_500ptv__450_500trptv")
    metBins["500_600ptv"] = ("500_600trptv", "500_600ptv__500_600trptv")
    metBins["600_800ptv"] = ("600_800trptv", "600_800ptv__600_800trptv")
    metBins["800ptv"] = ("__800trptv", "800ptv__800trptv")

    #metBins = metBins.OrderedDict(sorted(metBins.items()))
    outfile_Zhad = open("%shad_acceff2.txt" % particleName, 'w')

    #outfile_Zhad.write("m(DM)/[GeV] m(MM)/[GeV] xsec/[pb] ")
    outfile_Zhad.write("m(DM)/[GeV] & m(MM)/[GeV] ")
    for metbin in sorted(metBins.keys()):
        # if "500_600" in metbin or "800" in metbin:
        #outfile_Zhad.write("	nevt 	")
        outfile_Zhad.write(" %s  " % (metbin))
    outfile_Zhad.write("  &  total    ")
    #outfile_Zhad.write("  &  preSel     ")
    outfile_Zhad.write("\n")

    for signal in signalsZhad.keys():

        file0 = ROOT.TFile("../output/monoWZjj_HIGG5D1_v28_acceff_unblinded.root","READ")
        #file0 = ROOT.TFile("/data/silo02/users/ssuchek/FitInput-v28/MonoVfittingInput2017-09-25/monoV_13TeV_0lep_mj.root","READ")

        file0.cd()
        dataList = ROOT.gDirectory.GetListOfKeys()

        #nevt = 0
        numerator["150_200ptv"] = 0.0
        numerator["200_250ptv"] = 0.0
        numerator["250_300ptv"] = 0.0
        numerator["300_350ptv"] = 0.0
        numerator["350_400ptv"] = 0.0
        numerator["400_450ptv"] = 0.0
        numerator["450_500ptv"] = 0.0
        numerator["500_600ptv"] = 0.0
        numerator["600_800ptv"] = 0.0
        numerator["800ptv"] = 0.0

        denominator["150_200ptv"] = 0.0
        denominator["200_250ptv"] = 0.0
        denominator["250_300ptv"] = 0.0
        denominator["300_350ptv"] = 0.0
        denominator["350_400ptv"] = 0.0
        denominator["400_450ptv"] = 0.0
        denominator["450_500ptv"] = 0.0
        denominator["500_600ptv"] = 0.0
        denominator["600_800ptv"] = 0.0
        denominator["800ptv"] = 0.0

        numerator_allBins_afterSel = 0.0
        numerator_allBins_preSel = 0.0

        numerator_value = dict(zip(numerator.keys(), [float(value) for value in numerator.values()]))
        denominator_value = dict(zip(denominator.keys(), [float(value) for value in denominator.values()]))

        for key in dataList:

    	    TTname = key.GetName()

            if not signal in TTname: 
                continue

            if "reco_MET_afterSel" in TTname: 
                temp = file0.Get(TTname) 
                temp.SetName(TTname+"allBins_after")
                numerator_allBins_afterSel += temp.Integral()

            if "reco_MET_preSel" in TTname: 
                temp = file0.Get(TTname) 
                temp.SetName(TTname+"allBins_pre")
                numerator_allBins_preSel += temp.Integral()

            for metbin in metBins.keys(): 
                if metBins[metbin][0] in TTname and "reco_MET_preSel" in TTname:
                    temp = file0.Get(TTname) 
                    temp.SetName(TTname+"denom")
                    denominator_value[metbin] += temp.Integral()

                if metBins[metbin][1] in TTname and "reco_MET_afterSel" in TTname:
                    temp = file0.Get(TTname) 
                    temp.SetName(TTname+"num")
                    numerator_value[metbin] += temp.Integral()

        #outfile_Zhad.write(" %s  %s  %s     "  % (signalsZhad[signal][0], signalsZhad[signal][1], signalsZhad[signal][2]))
        outfile_Zhad.write(" %s  %s  "  % (signalsZhad[signal][0], signalsZhad[signal][1]))

        for metbin in sorted(metBins.keys()): 
            if denominator_value[metbin] == 0:
                print "%s (%s): %f / %f" % (signal, metbin, numerator_value[metbin], denominator_value[metbin])
            try:
                acceff = float(numerator_value[metbin])/(float(denominator_value[metbin]))
            except ZeroDivisionError:
                acceff = 0

            #if "5400" in metbin:
            outfile_Zhad.write(" %.2f/%.2f & " % (numerator_value[metbin], denominator_value[metbin]))
            #outfile_Zhad.write(" %.2f%% " % (100*acceff))
            #else:
                #outfile_Zhad.write(" %.2f%% " % (100*acceff))

        outfile_Zhad.write(" %.2f%% " % (100*numerator_allBins_afterSel/(float(signalsZhad[signal][2])*36100)))
        #outfile_Zhad.write(" & %.2f%%" % (100*numerator_allBins_preSel/(float(signalsZhad[signal][2])*36100)))

        outfile_Zhad.write("\n")

        #outfile_Zhad.write("m(DM) = %-5s GeV, MM = %-5s GeV: nevents = %-8.2f	xsec = %-12s pb	  AccxEff = %-5.2f %%\n" % (signalsZhad[signal][0], signalsZhad[signal][1], nevt, signalsZhad[signal][2], 100*nevt/(float(signalsZhad[signal][2])*36100)))

    outfile_Zhad.close()

    outfile_Zhad_in = "%shad_acceff2.txt" % particleName
    outfile_Zhad_out = open("%shad_acceff3.txt" % particleName, "w")

    os.system("sort -n -k 1 -k 2 %s -o %s" % (outfile_Zhad_in, outfile_Zhad_in))

    # with open(outfile_Zhad_in, "r+") as f:
    #     for line in f:
    #         data = line.split()
    #         for i in range(len(data)):
    #             outfile_Zhad_out.write( ('{0[%d]:<15}' % i).format(data))

    # with open(outfile_Zhad_f) as f:
    #     for line in f:
    #         data = line.split()
    #         f.write('{0[0]:<15}{0[1]:<15}{0[2]:<5}{0[3]:<15}{0[4]:>15}{0[5]:>15}{0[6]:>15}{0[7]:>15}{0[8]:>15}{0[9]:>15}{0[10]:>15}{0[11]:>15}'.format(data)

if __name__ == "__main__":
    main()
